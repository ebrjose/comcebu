<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'comcebu' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '123123' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+$uCffooxpo*Q8z5qyH.?n*jHvrxX:hdO&XcL+EuOT{*5VRxN~W-2T`at@Ddi*.U' );
define( 'SECURE_AUTH_KEY',  'oXuuPMdJI4w!*lm,XgHcfCc&{+5Ak7;KPlv=!I84&(r{n!D+e!rC+K<Reu%XcU<f' );
define( 'LOGGED_IN_KEY',    'o@iIVJ!?j`mC/3rkp`}l iEC<m^*4??F0Ayu3_Y iSA9Cl|w;G%R8[*6XkRO{2i-' );
define( 'NONCE_KEY',        'n:yL6[S?o8YlMII?mk|X<D|CAx&tfm5I^$=m79mj%DH7@lvKJ-jZ`%Gj+4^r$vIN' );
define( 'AUTH_SALT',        '*,VA{LO4`f9tcwJ!Rzg5aYOTmMDK N!Npw,B{HS![67,l=:NjuX.ovmS@jYFw?tF' );
define( 'SECURE_AUTH_SALT', 'fnm4/!2kg2Mqk7hM a[BAirS;OV3w0rBu*C^T(KvDy *NI,LXo@O*=0!)y@1_4X_' );
define( 'LOGGED_IN_SALT',   '3,K`4ZTLEgwW|mY69whXL^]auq*so}h,w&+675OmF.K]T{!Q6>#$V>zPSCGe<hA$' );
define( 'NONCE_SALT',       'X`z3Vr^/E-6>e%5@BnLb{dYk&/yH9X|}]VYIHz#H!1Slh4,`_uFQiMdMj$u`RN]L' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
