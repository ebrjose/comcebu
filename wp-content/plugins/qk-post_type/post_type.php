<?php 
/**
 * Plugin Name: QK Register Post Type
 * Description: This plugin register all post type come with theme.
 * Version: 1.0
 * Author: Quannt
 * Author URI: http://qkthemes.com
 */
?>
<?php

//Speaker

add_action( 'init', 'codex_speaker_init' );
/**
 * Register a speaker post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_speaker_init() {
	
	
	$labels = array(
		'name'               => __( 'Speakers', 'post type general name', 'qktheme' ),
		'singular_name'      => __( 'Speaker', 'post type singular name', 'qktheme' ),
		'menu_name'          => __( 'Speakers', 'admin menu', 'qktheme' ),
		'name_admin_bar'     => __( 'Speaker', 'add new on admin bar', 'qktheme' ),
		'add_new'            => __( 'Add New', 'speaker', 'qktheme' ),
		'add_new_item'       => __( 'Add New Speaker', 'qktheme' ),
		'new_item'           => __( 'New Speaker', 'qktheme' ),
		'edit_item'          => __( 'Edit Speaker', 'qktheme' ),
		'view_item'          => __( 'View Speaker', 'qktheme' ),
		'all_items'          => __( 'All Speakers', 'qktheme' ),
		'search_items'       => __( 'Search Speakers', 'qktheme' ),
		'parent_item_colon'  => __( 'Parent Speakers:', 'qktheme' ),
		'not_found'          => __( 'No speakers found.', 'qktheme' ),
		'not_found_in_trash' => __( 'No speakers found in Trash.', 'qktheme' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'menu_icon' 		 => 'dashicons-megaphone',
		'publicly_queryable' => true,
		'menu_position' 	 => 2,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'speaker' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail','excerpt')
	);

	register_post_type( 'speaker', $args );
}

// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_speaker_taxonomies', 0 );

// create two taxonomies, genres and writers for the post type "book"
function create_speaker_taxonomies() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => __( 'Speaker Categories', 'qktheme' ),
		'singular_name'     => __( 'Speaker Category', 'qktheme' ),
		'search_items'      => __( 'Search Speaker Categories','qktheme' ),
		'all_items'         => __( 'All Speaker Categories','qktheme' ),
		'parent_item'       => __( 'Parent Speaker Category','qktheme' ),
		'parent_item_colon' => __( 'Parent Speaker Category:','qktheme' ),
		'edit_item'         => __( 'Edit Speaker Category','qktheme' ),
		'update_item'       => __( 'Update Speaker Category','qktheme' ),
		'add_new_item'      => __( 'Add New Speaker Category','qktheme' ),
		'new_item_name'     => __( 'New Speaker Category Name','qktheme' ),
		'menu_name'         => __( 'Speaker Category' ,'qktheme'),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'speaker_category' ),
	);

	register_taxonomy( 'speaker_category', array( 'speaker' ), $args );


}



//Schedule


add_action( 'init', 'codex_schedule_init' );
/**
 * Register a schedule post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_schedule_init() {
	
	
	$labels = array(
		'name'               => __( 'Schedules', 'post type general name', 'qktheme' ),
		'singular_name'      => __( 'Schedule', 'post type singular name', 'qktheme' ),
		'menu_name'          => __( 'Schedules', 'admin menu', 'qktheme' ),
		'name_admin_bar'     => __( 'Schedule', 'add new on admin bar', 'qktheme' ),
		'add_new'            => __( 'Add New', 'schedule', 'qktheme' ),
		'add_new_item'       => __( 'Add New Schedule', 'qktheme' ),
		'new_item'           => __( 'New Schedule', 'qktheme' ),
		'edit_item'          => __( 'Edit Schedule', 'qktheme' ),
		'view_item'          => __( 'View Schedule', 'qktheme' ),
		'all_items'          => __( 'All Schedules', 'qktheme' ),
		'search_items'       => __( 'Search Schedules', 'qktheme' ),
		'parent_item_colon'  => __( 'Parent Schedules:', 'qktheme' ),
		'not_found'          => __( 'No schedules found.', 'qktheme' ),
		'not_found_in_trash' => __( 'No schedules found in Trash.', 'qktheme' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'menu_icon' 		 => 'dashicons-calendar-alt',
		'publicly_queryable' => true,
		'menu_position' 	 => 2,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'schedule' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail','excerpt')
	);

	register_post_type( 'schedule', $args );
}

// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_schedule_taxonomies', 0 );

// create two taxonomies, genres and writers for the post type "book"
function create_schedule_taxonomies() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => __( 'Schedule Categories', 'qktheme' ),
		'singular_name'     => __( 'Schedule Category', 'qktheme' ),
		'search_items'      => __( 'Search Schedule Categories','qktheme' ),
		'all_items'         => __( 'All Schedule Categories','qktheme' ),
		'parent_item'       => __( 'Parent Schedule Category','qktheme' ),
		'parent_item_colon' => __( 'Parent Schedule Category:','qktheme' ),
		'edit_item'         => __( 'Edit Schedule Category','qktheme' ),
		'update_item'       => __( 'Update Schedule Category','qktheme' ),
		'add_new_item'      => __( 'Add New Schedule Category','qktheme' ),
		'new_item_name'     => __( 'New Schedule Category Name','qktheme' ),
		'menu_name'         => __( 'Schedule Category' ,'qktheme'),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'schedule_category' ),
	);

	register_taxonomy( 'schedule_category', array( 'schedule' ), $args );


}



//Pricing


add_action( 'init', 'codex_pricing_init' );
/**
 * Register a pricing post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_pricing_init() {
	
	
	$labels = array(
		'name'               => __( 'Pricings', 'post type general name', 'qktheme' ),
		'singular_name'      => __( 'Pricing', 'post type singular name', 'qktheme' ),
		'menu_name'          => __( 'Pricings', 'admin menu', 'qktheme' ),
		'name_admin_bar'     => __( 'Pricing', 'add new on admin bar', 'qktheme' ),
		'add_new'            => __( 'Add New', 'pricing', 'qktheme' ),
		'add_new_item'       => __( 'Add New Pricing', 'qktheme' ),
		'new_item'           => __( 'New Pricing', 'qktheme' ),
		'edit_item'          => __( 'Edit Pricing', 'qktheme' ),
		'view_item'          => __( 'View Pricing', 'qktheme' ),
		'all_items'          => __( 'All Pricings', 'qktheme' ),
		'search_items'       => __( 'Search Pricings', 'qktheme' ),
		'parent_item_colon'  => __( 'Parent Pricings:', 'qktheme' ),
		'not_found'          => __( 'No pricings found.', 'qktheme' ),
		'not_found_in_trash' => __( 'No pricings found in Trash.', 'qktheme' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'menu_icon' 		 => 'dashicons-editor-ul',
		'publicly_queryable' => true,
		'menu_position' 	 => 2,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'pricing' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', )
	);

	register_post_type( 'pricing', $args );
}

// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_pricing_taxonomies', 0 );

// create two taxonomies, genres and writers for the post type "book"
function create_pricing_taxonomies() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => __( 'Pricing Categories', 'qktheme' ),
		'singular_name'     => __( 'Pricing Category', 'qktheme' ),
		'search_items'      => __( 'Search Pricing Categories','qktheme' ),
		'all_items'         => __( 'All Pricing Categories','qktheme' ),
		'parent_item'       => __( 'Parent Pricing Category','qktheme' ),
		'parent_item_colon' => __( 'Parent Pricing Category:','qktheme' ),
		'edit_item'         => __( 'Edit Pricing Category','qktheme' ),
		'update_item'       => __( 'Update Pricing Category','qktheme' ),
		'add_new_item'      => __( 'Add New Pricing Category','qktheme' ),
		'new_item_name'     => __( 'New Pricing Category Name','qktheme' ),
		'menu_name'         => __( 'Pricing Category' ,'qktheme'),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'pricing_category' ),
	);

	register_taxonomy( 'pricing_category', array( 'pricing' ), $args );


}


?>