<?php get_header(); ?>

<!-- blog-section 
			================================================== -->
		<section class="blog-section">
			<div class="container">
				
				<div class="row">
					<div class="col-lg-8">
						<div class="blog-box">
							<div class="blog-post single-post">

								<div class="single-content">
									<?php woocommerce_content(); ?>
				                  </div>
								
							</div>
							

							

							
							<!-- End contact form box -->
						</div>
					</div>
					<div class="col-lg-4">
						
						<?php get_sidebar(); ?>

					</div>
				</div>
			</div>
		</section>
		<!-- End blog section -->


<?php get_footer(); ?>