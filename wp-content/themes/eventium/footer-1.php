<!-- footer
			================================================== -->
<footer>
	<div class="container">
		<?php

		$active = 0;

		if (is_active_sidebar('footer-1')) {
			$active = $active + 1;
		}

		if (is_active_sidebar('footer-2')) {
			$active = $active + 1;
		}

		if (is_active_sidebar('footer-3')) {
			$active = $active + 1;
		}

		if (is_active_sidebar('footer-4')) {
			$active = $active + 1;
		}

		if ($active > 0) {
			$column = 12 / $active;
		}

		?>
		<?php if ($active > 0) { ?>
			<div class="up-footer">
				<div class="row">
					<?php if (is_active_sidebar('footer-1')) { ?>

						<div class="col-md-<?php echo esc_attr($column); ?>">
							<?php dynamic_sidebar('footer-1'); ?>
						</div>

					<?php } ?>

					<?php if (is_active_sidebar('footer-2')) { ?>
						<div class="col-md-<?php echo esc_attr($column); ?>">
							<?php dynamic_sidebar('footer-2'); ?>

						</div>
					<?php } ?>

					<?php if (is_active_sidebar('footer-3')) { ?>
						<div class="col-md-<?php echo esc_attr($column); ?>">
							<?php dynamic_sidebar('footer-3'); ?>


						</div>
					<?php } ?>

				</div>
			</div>
		<?php } ?>
		<p class="copyright-line"><?php echo do_shortcode(get_theme_mod('copyright', '&copy; COMCEBU - ' . date('Y'))); ?></p>

	</div>
</footer>
<!-- End footer -->