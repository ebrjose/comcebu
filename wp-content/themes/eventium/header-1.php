<!-- Header
		    ================================================== -->
		<header class="clearfix second-style">

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container">

					<?php $logo = get_theme_mod( 'logo', get_template_directory_uri().'/images/logo-black@2x.png'); ?>
					
					<a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo esc_url($logo); ?>" alt="<?php bloginfo('name'); ?>"></a>


					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<?php 
							
							$defaults2= array(
								'theme_location'  => 'primary',
								'menu'            => '',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'navbar-nav ml-auto',
								'menu_id'         => '',
								'echo'            => true,
								 'fallback_cb'       => 'eventium_bootstrap_navwalker::fallback',
								 'walker'            => new eventium_bootstrap_navwalker(),
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 0,
							);
							if ( has_nav_menu( 'primary' ) ) {
								wp_nav_menu( $defaults2 );
							}
						
						?>
						
					</div>
				</div>
			</nav>

		</header>
		<!-- End Header -->