<?php get_header(); ?>

	<?php while(have_posts()) : the_post(); ?>
		<!-- speaking-single
			================================================== -->
		<section class="speaking-single">
			<div class="container">
				<div class="single-box">

					<div class="row">
						<div class="col-lg-5 col-md-6">
							<div class="single-image">
								<a href="#"><i class="fa fa-angle-left"></i> Back</a>
								<?php the_post_thumbnail(); ?>
							</div>
						</div>
						<div class="col-lg-6 offset-lg-1 col-md-6">
							<div class="single-content">
								<div class="single-data">
									<h1 class="heading1"><?php the_title(); ?></h1>
									<?php if(eventium_option('job')!=''){ ?>
									<span><?php echo esc_html(eventium_option('job')); ?></span>
									<?php } ?>
									<?php the_content(); ?>
								</div>

								<div class="single-connection">
									<h2 class="heading2"><?php esc_html_e('Connection','eventium'); ?>:</h2>
									<ul class="social-links">
										<?php if(eventium_option('facebook')!=''){ ?>
										<li><a class="facebook" href="<?php echo esc_url(eventium_option('facebook')); ?>"><i class="fa fa-facebook"></i></a></li>
										<?php } ?>
										<?php if(eventium_option('twitter')!=''){ ?>
										<li><a class="twitter" href="<?php echo esc_url(eventium_option('twitter')); ?>"><i class="fa fa-twitter"></i></a></li>
										<?php } ?>
										<?php if(eventium_option('linkedin')!=''){ ?>
										<li><a class="linkedin" href="<?php echo esc_url(eventium_option('linkedin')); ?>"><i class="fa fa-linkedin"></i></a></li>
										<?php } ?>
										<?php if(eventium_option('instagram')!=''){ ?>
										<li><a class="personal-web" href="<?php echo esc_url(eventium_option('instagram')); ?>"><i class="fa fa-instagram"></i></a></li>
										<?php } ?>
										<?php if(eventium_option('pinterest')!=''){ ?>
										<li><a class="personal-web" href="<?php echo esc_url(eventium_option('pinterest')); ?>"><i class="fa fa-pinterest"></i></a></li>
										<?php } ?>
									</ul>
									<p>
										<?php if(eventium_option('phone')!=''){ ?>
										<?php esc_html_e('Phone','eventium'); ?>: <span><?php echo esc_html(eventium_option('phone')); ?></span> <br>
										<?php } ?>
										<?php if(eventium_option('email')!=''){ ?>
										<?php esc_html_e('Email','eventium'); ?>: <span><?php echo esc_html(eventium_option('email')); ?></span>
										<?php } ?>
									</p>
								</div>
								<?php $skills = eventium_option('skills'); ?>
								<?php if(count($skills)>0){ ?>
								<div class="single-skills">
									<h2><?php esc_html_e('Skills','eventium'); ?>:</h2>
									
									<div class="skills-progress">
										<?php foreach ( (array) $skills as $key => $skills ) { ?>
										<p><?php echo esc_html($skills['_eventium_stitle']); ?> - <?php echo esc_html($skills['_eventium_slevel']); ?></p>
										<div class="meter nostrips">
											<p style="width: <?php echo esc_attr($skills['_eventium_slevel']); ?>"></p>
										</div>
										<?php } ?>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<!-- End speaking single -->
	<?php endwhile; ?>

<?php get_footer(); ?>