<?php get_header(); ?>
<?php while(have_posts()) : the_post(); ?>
<!-- blog-section 
			================================================== -->
		<section class="blog-section">
			<div class="container">
				<div class="title-section">
					<h1 class="heading1"><?php the_title(); ?></h1>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<div class="blog-box">
							<div class="blog-post single-post">

								<div class="single-content">
									<?php the_post_thumbnail(); ?>
									<?php the_content(); ?>
				                      <?php
				                        $defaults = array(
				                          'before'           => '<div id="page-links"><strong>'.esc_html__('Page','eventium').': </strong>',
				                          'after'            => '</div>',
				                          'link_before'      => '<span>',
				                          'link_after'       => '</span>',
				                          'next_or_number'   => 'number',
				                          'separator'        => ' ',
				                          'nextpagelink'     => esc_html__( 'Next page','eventium' ),
				                          'previouspagelink' => esc_html__( 'Previous page','eventium' ),
				                          'pagelink'         => '%',
				                          'echo'             => 1
				                        );
				                       ?>
				                      <?php wp_link_pages($defaults); ?>
				                  </div>
								
							</div>
							

							<?php if ( comments_open() || get_comments_number() ) {  ?>
			                    <?php comments_template(); ?>   
			                <?php } ?>

							
							<!-- End contact form box -->
						</div>
					</div>
					<div class="col-lg-4">
						
						<?php get_sidebar(); ?>

					</div>
				</div>
			</div>
		</section>
		<!-- End blog section -->
<?php endwhile; ?>

<?php get_footer(); ?>