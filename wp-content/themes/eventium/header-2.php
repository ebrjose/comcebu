<header class="clearfix">

			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
				<div class="container-fluid">
					<?php $logo = get_theme_mod( 'logo2', get_template_directory_uri().'/images/logo@2x.png'); ?>
					<a class="navbar-brand"  href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo esc_url($logo); ?>" alt="<?php bloginfo('name'); ?>">
					</a>

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<?php 
							$onepage = eventium_option($option='onepage');
							if($onepage=='on'){
								$menu_location = 'onepage';
							}else{
								$menu_location = 'primary';
							}
							$defaults2= array(
								'theme_location'  => $menu_location,
								'menu'            => '',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'navbar-nav m-auto',
								'menu_id'         => '',
								'echo'            => true,
								 'fallback_cb'       => 'eventium_bootstrap_navwalker::fallback',
								 'walker'            => new eventium_bootstrap_navwalker(),
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 0,
							);
							if ( has_nav_menu( $menu_location ) ) {
								wp_nav_menu( $defaults2 );
							}
						
						?>
						<?php if(get_theme_mod('buy_ticket')!=''){ ?>
							<a href="<?php echo esc_url(get_theme_mod('buy_ticket')); ?>" class="default-button"><?php echo esc_html(get_theme_mod('buy_text','Buy Ticket')); ?></a>
						<?php } ?>
					</div>
				</div>
			</nav>

		</header>
		<!-- End Header -->
