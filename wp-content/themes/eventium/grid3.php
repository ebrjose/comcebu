<div <?php post_class('blog-post'); ?>>
	<span class="date-comments"><?php the_time(get_option('date_format')); ?>, <?php
																					comments_popup_link(esc_html__('0 Comments', 'eventium'), esc_html__('1 Comment', 'eventium'), esc_html__('% Comments', 'eventium'), '',  esc_html__('Comment off', 'eventium'));
																					?></span>
	<h2 class="heading2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	<?php the_post_thumbnail('grid'); ?>
	<?php the_excerpt(); ?>
	<a href="<?php the_permalink(); ?>" class="continue-read"><?php esc_html_e('Seguir leyendo', 'eventium'); ?> <i class="fa fa-angle-right"></i></a>
</div>