<div class="blog-post quote-post">
	<blockquote>
		<span class="date-comments"><?php the_time(get_option( 'date_format' )); ?>, <?php
             comments_popup_link( esc_html__('0 Comments','eventium'), esc_html__('1 Comment','eventium'), esc_html__('% Comments','eventium'), '',  esc_html__('Comment off','eventium'));
            ?></span>
		<?php the_content(); ?>
	</blockquote>
</div>