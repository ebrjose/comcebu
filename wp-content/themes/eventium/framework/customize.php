<?php

/**
 * An example file demonstrating how to add all controls.
 *
 * @package     Kirki
 * @category    Core
 * @author      Aristeides Stathopoulos
 * @copyright   Copyright (c) 2017, Aristeides Stathopoulos
 * @license     http://opensource.org/licenses/https://opensource.org/licenses/MIT
 * @since       3.0.12
 */
// Exit if accessed directly.
if (!defined('ABSPATH')) {
	exit;
}
// Do not proceed if Kirki does not exist.
if (!class_exists('Kirki')) {
	return;
}
/**
 * First of all, add the config.
 *
 * @link https://aristath.github.io/kirki/docs/getting-started/config.html
 */
Kirki::add_config(
	'kirki_demo',
	array(
		'capability'  => 'edit_theme_options',
		'option_type' => 'theme_mod',
	)
);
/**
 * Add a panel.
 *
 * @link https://aristath.github.io/kirki/docs/getting-started/panels.html
 */
Kirki::add_panel(
	'eventium_options_panel',
	array(
		'priority'    => 10,
		'title'       => esc_attr__('Eventium Options Panel', 'eventium'),
		'description' => esc_attr__('Config Your Theme Options Here.', 'eventium'),
	)
);
/**
 * Add Sections.
 *
 * We'll be doing things a bit differently here, just to demonstrate an example.
 * We're going to define 1 section per control-type just to keep things clean and separate.
 *
 * @link https://aristath.github.io/kirki/docs/getting-started/sections.html
 */
$sections = array(
	'body'      => array(esc_attr__('Main Body Style', 'eventium'), ''),
	'header'      => array(esc_attr__('Header Settings', 'eventium'), ''),
	'blog'      => array(esc_attr__('BLog Settings', 'eventium'), ''),
	'portfolio'      => array(esc_attr__('Portfolio Settings', 'eventium'), ''),
	'copyright'      => array(esc_attr__('Footer Settings', 'eventium'), ''),
	'typo'      => array(esc_attr__('Typography Settings', 'eventium'), ''),


);
foreach ($sections as $section_id => $section) {
	$section_args = array(
		'title'       => $section[0],
		'description' => $section[1],
		'panel'       => 'eventium_options_panel',
	);
	if (isset($section[2])) {
		$section_args['type'] = $section[2];
	}
	Kirki::add_section(str_replace('-', '_', $section_id) . '_section', $section_args);
}
/**
 * A proxy function. Automatically passes-on the config-id.
 *
 * @param array $args The field arguments.
 */
function my_config_kirki_add_field($args)
{
	Kirki::add_field('kirki_demo', $args);
}
/**
 * Background Control.
 *
 * @todo Triggers change on load.
 */
/**BLOG CONTROLS**/


my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'blog_title',
		'label'       => esc_attr__('Blog Heading Title', 'eventium'),
		'description' => esc_attr__('insert your blog default title', 'eventium'),
		'section'     => 'blog_section',
		'default'     => esc_html__('Showcase blog posts', 'eventium'),
	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'switch',
		'settings'    => 'related_post',
		'label'       => esc_attr__('Turn On Related Post Section', 'eventium'),
		'description' => esc_attr__('Turn On/Off Related Post Section Here', 'eventium'),
		'section'     => 'blog_section',
		'default'     => false,
	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'buy_ticket',
		'label'       => esc_attr__('Buy ticket link', 'eventium'),
		'description' => esc_attr__('insert your header buy ticket link', 'eventium'),
		'section'     => 'header_section',
	)
);
my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'buy_text',
		'label'       => esc_attr__('Buy ticket text', 'eventium'),
		'description' => esc_attr__('insert your header buy ticket text button', 'eventium'),
		'section'     => 'header_section',
	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'textarea',
		'settings'    => 'blog_subtitle',
		'label'       => esc_attr__('Blog Heading Subtitle', 'eventium'),
		'description' => esc_attr__('insert your blog heading description here.', 'eventium'),
		'section'     => 'blog_section',
		'default'     => '',
	)
);


my_config_kirki_add_field(
	array(
		'type'        => 'color',
		'settings'    => 'main_color',
		'label'       => esc_attr__('Chose Main Color 1', 'eventium'),
		'description' => esc_attr__('Chose your Main Color Style.', 'eventium'),
		'section'     => 'body_section',
		'default'     => '#5649c0',

	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'color',
		'settings'    => 'main_color2',
		'label'       => esc_attr__('Chose Main Color 2', 'eventium'),
		'description' => esc_attr__('Chose your Main Color Style.', 'eventium'),
		'section'     => 'body_section',
		'default'     => '#2ecc71',

	)
);

/**
 * HEADER Control.
 */

my_config_kirki_add_field(
	array(
		'type'        => 'image',
		'settings'    => 'logo',
		'label'       => esc_attr__('Logo', 'eventium'),
		'description' => esc_attr__('Upload your Logo Here.', 'eventium'),
		'section'     => 'header_section',
		'default'     => get_template_directory_uri() . '/images/logo-black.png',
	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'image',
		'settings'    => 'logo2',
		'label'       => esc_attr__('Logo for White Header', 'eventium'),
		'description' => esc_attr__('Upload your Logo Here.', 'eventium'),
		'section'     => 'header_section',
		'default'     => get_template_directory_uri() . '/images/logo.png',
	)
);

/**
 * HEADER Control.
 */
my_config_kirki_add_field(
	array(
		'type'        => 'select',
		'settings'    => 'header_layout',
		'label'       => esc_attr__('Header Style', 'eventium'),
		'description' => esc_attr__('Select Your header style.', 'eventium'),
		'section'     => 'header_section',
		'default'     => '1',
		'choices'     => array(
			'1'     => esc_attr__('Style 1', 'eventium'),
			'2'     => esc_attr__('Style 2', 'eventium'),
			'3'     => esc_attr__('Style 3', 'eventium'),
			'4' 	=> esc_attr__('Style 4', 'eventium'),
		),
	)
);
my_config_kirki_add_field(
	array(
		'type'        => 'select',
		'settings'    => 'footer_layout',
		'label'       => esc_attr__('Footer Style', 'eventium'),
		'description' => esc_attr__('Select Your footer style.', 'eventium'),
		'section'     => 'copyright_section',
		'default'     => '1',
		'choices'     => array(
			'1'     => esc_attr__('Style 1', 'eventium'),
			'2'     => esc_attr__('Style 2', 'eventium'),
			'3'     => esc_attr__('Style 3', 'eventium'),
		),
	)
);



/**Single Ad**/



my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'facebook_url',
		'label'       => esc_attr__('Facebook', 'eventium'),
		'description' => esc_attr__('insert your facebook url', 'eventium'),
		'section'     => 'copyright_section',
		'default'     => '',
	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'twitter_url',
		'label'       => esc_attr__('Twitter', 'eventium'),
		'description' => esc_attr__('insert your twitter url', 'eventium'),
		'section'     => 'copyright_section',
		'default'     => '',
	)
);
my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'youtube',
		'label'       => esc_attr__('Youtube', 'eventium'),
		'description' => esc_attr__('insert your google+ url', 'eventium'),
		'section'     => 'copyright_section',
		'default'     => '',
	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'instagram_url',
		'label'       => esc_attr__('Instagram', 'eventium'),
		'description' => esc_attr__('insert your instagram url', 'eventium'),
		'section'     => 'copyright_section',
		'default'     => '',
	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'linkedin_url',
		'label'       => esc_attr__('Linkedin', 'eventium'),
		'description' => esc_attr__('insert your linkedin url', 'eventium'),
		'section'     => 'copyright_section',
		'default'     => '',
	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'copyright',
		'label'       => esc_attr__('Copyright', 'eventium'),
		'description' => esc_attr__('insert your Copyright text', 'eventium'),
		'section'     => 'copyright_section',
		'default'     => esc_html__('&copy; Copyright By Nunforest 2018', 'eventium'),
	)
);
/****/
my_config_kirki_add_field(
	array(
		'type'        => 'typography',
		'settings'    => 'main_typo',
		'label'       => esc_attr__('Body Typography', 'eventium'),
		'section'     => 'typo_section',
		'default'     => array(
			'font-family'    => 'Rubik',
			'font-size'      => '15px',
			'line-height'    => '30px',
			'variant'    => 300,
			'color'          => '#6c7a89',

		),
		'priority'    => 10,
		'output'      => array(
			array(
				'element' => 'body, .about-event-box a, .paragraph, section.register-section .register-box #register-form .subcribe-bx span, .article-box span, .conference-features .center-area a, section.schedule-section3 .schedule-block .schedule-speakers ul.speaker-list li span, section.speaking-single .single-box .single-image a, section.blog-section .blog-box .blog-post span.date-comments, section.blog-section .blog-box .blog-post ul.post-tags li, .pagination-box .prev,
.pagination-box .next, p, section.news-section .news-box .news-post span, section.news-section2 .blog-post span.date-comments',
			),
		),
	)
);

my_config_kirki_add_field(
	array(
		'type'        => 'text',
		'settings'    => 'map_api',
		'label'       => esc_attr__('Google Map API', 'eventium'),
		'section'     => 'body_section',
		'default'     => 'AIzaSyCiqrIen8rWQrvJsu-7f4rOta0fmI5r2SI',
	)
);
