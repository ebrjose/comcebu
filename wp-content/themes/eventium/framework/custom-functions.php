<?php 

/* Header */
function eventium_header() {
    global $wp_query;
    $header_layout = get_post_meta($wp_query->get_queried_object_id(), '_eventium_header_layout', true);
    if($header_layout!='' and $header_layout!='1'){
        $header_layout = $header_layout;
    }else{ 
    $header_layout = get_theme_mod( 'header_layout', '1' );
    }

    if($header_layout!='' and $header_layout!='1'){
      $header_layout = $header_layout;
    }else{
      
        $header_layout = '1';
      
    }
   
   get_template_part('header', $header_layout);
}

/* Footer */
function eventium_footer() {
    global $wp_query;
    $footer_layout = get_post_meta($wp_query->get_queried_object_id(), '_eventium_footer_layout', true);
    if($footer_layout!='' and $footer_layout!='1'){
        $footer_layout = $footer_layout;
    }else{ 
    $footer_layout = get_theme_mod( 'footer_layout', '1' );
    }

    if($footer_layout!='' and $footer_layout!='1'){
      $footer_layout = $footer_layout;
    }else{
      
        $footer_layout = '1';
      
    }
   
   get_template_part('footer', $footer_layout);
}

?>