<?php


add_action('cmb2_admin_init', 'eventium_metaboxes');
/**
 * Define the metabox and field configurations.
 */
function eventium_metaboxes()
{

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_eventium_';

    /**
     * Initiate the metabox
     */

    $cmb = new_cmb2_box(array(
        'id'            => 'page_options',
        'title'         => esc_html__('Page Setting', 'eventium'),
        'object_types'  => array('page'), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left

    ));

    $cmb->add_field(array(
        'name'             => esc_html__('Page Header Style', 'eventium'),
        'desc'             => esc_html__('Select your page header style', 'eventium'),
        'id'               => $prefix . 'header_layout',
        'type'             => 'select',

        'options'          => array(
            '1' => esc_html__('Style 1', 'eventium'),
            '2' => esc_attr__('Style 2', 'eventium'),
            '3' => esc_attr__('Style 3', 'eventium'),
            '4' => esc_attr__('Style 4', 'eventium'),
        ),
    ));


    $cmb->add_field(array(
        'name'             => esc_html__('Page Footer Style', 'eventium'),
        'desc'             => esc_html__('Select your page footer style', 'eventium'),
        'id'               => $prefix . 'footer_layout',
        'type'             => 'select',

        'options'          => array(
            '1' => esc_html__('Style 1', 'eventium'),
            '2' => esc_attr__('Style 2', 'eventium'),
            '3' => esc_attr__('Style 3', 'eventium'),
        ),
    ));

    $cmb->add_field(array(
        'name' => esc_html__('One Page Menu', 'eventium'),
        'desc' => esc_html__('Use One Page Menu For this Page', 'eventium'),
        'id'   => $prefix . 'onepage',
        'type' => 'checkbox',
    ));

    $cmb = new_cmb2_box(array(
        'id'            => 'speaker_options',
        'title'         => esc_html__('Speaker Setting', 'eventium'),
        'object_types'  => array('speaker',), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left

    ));



    $cmb->add_field(array(
        'name' => esc_html__(' Job', 'eventium'),
        'desc' => '',
        'id'   => $prefix . 'job',
        'type'    => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__(' Phone', 'eventium'),
        'desc' => '',
        'id'   => $prefix . 'phone',
        'type'    => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__(' Email', 'eventium'),
        'desc' => '',
        'id'   => $prefix . 'email',
        'type'    => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Facebook URL', 'eventium'),
        'desc' => '',
        'id'   => $prefix . 'facebook',
        'type'    => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Twitter URL', 'eventium'),
        'desc' => '',
        'id'   => $prefix . 'twitter',
        'type'    => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Linkedin URL', 'eventium'),
        'desc' => '',
        'id'   => $prefix . 'linkedin',
        'type'    => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Instagram URL', 'eventium'),
        'desc' => '',
        'id'   => $prefix . 'instagram',
        'type'    => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Pinterest URL', 'eventium'),
        'desc' => '',
        'id'   => $prefix . 'pinterest',
        'type'    => 'text',

    ));

    $group_field_id = $cmb->add_field(array(
        'id'          => $prefix . 'skills',
        'type'        => 'group',
        'description' => esc_html__('Speaker Skills', 'eventium'),
        // 'repeatable'  => false, // use false if you want non-repeatable group
        'options'     => array(
            'skill_name'   => esc_html__('Entry {#}', 'eventium'), // since version 1.1.4, {#} gets replaced by row number
            'skill_level'    => esc_html__('Add Another Entry', 'eventium'),
            'sortable'      => true, // beta
            // 'closed'     => true, // true to have the groups closed by default
        ),
    ));
    $cmb->add_group_field($group_field_id, array(
        'name' => esc_html__('Skill Name', 'eventium'),
        'id'   => $prefix . 'stitle',
        'type' => 'text',
        // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ));
    $cmb->add_group_field($group_field_id, array(
        'name' => esc_html__('Skill Level', 'eventium'),
        'id'   => $prefix . 'slevel',
        'type' => 'text',
        // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ));

    $cmb = new_cmb2_box(array(
        'id'            => 'pricing_options',
        'title'         => esc_html__('Pricing Setting', 'eventium'),
        'object_types'  => array('pricing',), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Price', 'eventium'),
        'desc' => '',
        'id'   => '_price',
        'type'    => 'text',

    ));
    $cmb->add_field(array(
        'name' => esc_html__('Buy Ticket Link', 'eventium'),
        'desc' => '',
        'id'   => 'purchase_link',
        'type'    => 'text',
        'default' => '#'

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Enterprise', 'eventium'),
        'desc' => esc_html__('Set this as Enterprise ticket', 'eventium'),
        'id'   => $prefix . 'enterprise',
        'type' => 'checkbox',
    ));

    $group_field_id = $cmb->add_field(array(
        'id'          => $prefix . 'list',
        'type'        => 'group',
        'description' => esc_html__('Pricing List', 'eventium'),
        // 'repeatable'  => false, // use false if you want non-repeatable group

    ));
    $cmb->add_group_field($group_field_id, array(
        'name' => esc_html__('List item', 'eventium'),
        'id'   => $prefix . 'litem',
        'type' => 'text',
        // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ));

    $cmb = new_cmb2_box(array(
        'id'            => 'schedule_options',
        'title'         => esc_html__('Schedule Setting', 'eventium'),
        'object_types'  => array('schedule',), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left

    ));



    $cmb->add_field(array(
        'name' => esc_html__('Time', 'eventium'),
        'desc' => '',
        'id'   => $prefix . 'time',
        'type'    => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Hora de Comer', 'eventium'),
        'desc' => esc_html__('Set this as Hora de Comer', 'eventium'),
        'id'   => $prefix . 'lunch',
        'type' => 'checkbox',
    ));

    // $cmb->add_field(array(
    //     'name' => esc_html__('After Party!', 'eventium'),
    //     'desc' => esc_html__('Set this as After time', 'eventium'),
    //     'id'   => $prefix . 'after',
    //     'type' => 'checkbox',
    // ));

    $cmb->add_field(array(
        'name' => esc_html__('Visita a Nelore', 'eventium'),
        'desc' => esc_html__('Set this as Visita a Nelore', 'eventium'),
        'id'   => $prefix . 'nelore',
        'type' => 'checkbox',
    ));

    $cmb->add_field(array(
        'name'             => esc_html__('Speaker', 'eventium'),
        'desc'             => esc_html__('Select speaker for this time', 'eventium'),
        'id'               => $prefix . 'speaker',
        'type'             => 'select',

        'options'          => eventium_get_speaker_post_options(),
    ));

    $cmb = new_cmb2_box(array(
        'id'            => 'user_edit',
        'title'         => esc_html__('User Meta Profile', 'eventium'),
        'object_types'  => array('user',), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left

    ));
    $cmb->add_field(
        array(
            'name'    => esc_html__('Avatar', 'eventium'),
            'desc'    => esc_html__('field description (optional)', 'eventium'),
            'id'      => $prefix . 'avatar',
            'type'    => 'file',
            'save_id' => true,
        )
    );
    $cmb->add_field(
        array(
            'name'     => esc_html__('Facebook', 'eventium'),
            'desc'     => esc_html__('Your Facebook Link (optional)', 'eventium'),
            'id'       => $prefix . 'user_facebook',
            'type'     => 'text',
            'on_front' => false,
        )
    );
    $cmb->add_field(

        array(
            'name'     => esc_html__('Google', 'eventium'),
            'desc'     => esc_html__('Your Google Link (optional)', 'eventium'),
            'id'       => $prefix . 'user_google',
            'type'     => 'text',
            'on_front' => false,
        )
    );
    $cmb->add_field(
        array(
            'name'     => esc_html__('Twitter', 'eventium'),
            'desc'     => esc_html__('Your Twitter Link (optional)', 'eventium'),
            'id'       => $prefix . 'user_twitter',
            'type'     => 'text',
            'on_front' => false,
        )
    );
    $cmb->add_field(
        array(
            'name'     => esc_html__('Youtube', 'eventium'),
            'desc'     => esc_html__('Your Youtube Link (optional)', 'eventium'),
            'id'       => $prefix . 'user_youtube',
            'type'     => 'text',
            'on_front' => false,
        )
    );
    $cmb->add_field(
        array(
            'name'     => esc_html__('Linkedin', 'eventium'),
            'desc'     => esc_html__('Your Linkedin Link (optional)', 'eventium'),
            'id'       => $prefix . 'user_linkedin',
            'type'     => 'text',
            'on_front' => false,
        )
    );
    $cmb->add_field(
        array(
            'name'     => esc_html__('Instagram', 'eventium'),
            'desc'     => esc_html__('Your Instagram Link (optional)', 'eventium'),
            'id'       => $prefix . 'user_instagram',
            'type'     => 'text',
            'on_front' => false,
        )
    );
    $cmb->add_field(
        array(
            'name'     => esc_html__('Dribbble', 'eventium'),
            'desc'     => esc_html__('Your Dribbble Link (optional)', 'eventium'),
            'id'       => $prefix . 'user_dribble',
            'type'     => 'text',
            'on_front' => false,
        )
    );
}
