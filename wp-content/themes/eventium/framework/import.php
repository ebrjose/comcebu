<?php 
function eventium_import_files() {
  return array(
    array(
      'import_file_name'           => esc_html__('Default','eventium'),
      'categories'                 => array( esc_html__('Demo Data','eventium') ),
      'import_file_url'            => get_template_directory_uri() . '/framework/default/content.xml',
      'import_widget_file_url'     => get_template_directory_uri() . '/framework/default/widgets.json',
      'local_import_customizer_file' => get_template_directory_uri() . '/framework/default/customizer.dat',
      'import_preview_image_url'   => get_template_directory_uri() . '/screenshot.png',
      'import_notice'              => esc_html__( 'After you import this demo, you will have to setup the slider separately.', 'eventium' ),
    )
    
  );
}
add_filter( 'pt-ocdi/import_files', 'eventium_import_files' );
?>