<?php
/**
 * Implements the compatibility for the Elementor plugin in Eventium  theme.
 *
 * @package    Nunforest
 * @subpackage Eventium 
 * @since      Eventium  1.2.3
 */


if ( ! function_exists( 'eventium_elementor_active_page_check' ) ) :

	/**
	 * Check whether Elementor plugin is activated and is active on current page or not
	 *
	 * @return bool
	 *
	 * @since Eventium  1.2.3
	 */
	function eventium_elementor_active_page_check() {
		global $post;

		if ( defined( 'ELEMENTOR_VERSION' ) && get_post_meta( $post->ID, '_elementor_edit_mode', true ) ) {
			return true;
		}

		return false;
	}

endif;

if ( ! function_exists( 'eventium_elementor_widget_render_filter' ) ) :

	/**
	 * Render the default WordPress widget settings, ie, divs
	 *
	 * @param $args the widget id
	 *
	 * @return array register sidebar divs
	 *
	 * @since Eventium  1.2.3
	 */
	function eventium_elementor_widget_render_filter( $args ) {

		return
			array(
				'before_widget' => '<section class="widget ' . eventium_widget_class_names( $args[ 'widget_id' ] ) . ' clearfix">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="heading2 widget-title">',
				'after_title'   => '</h2>',
			);
	}

endif;

add_filter( 'elementor/widgets/wordpress/widget_args', 'eventium_elementor_widget_render_filter' ); // WPCS: spelling ok.

if ( ! function_exists( 'eventium_widget_class_names' ) ) :

	/**
	 * Render the widget classes for Elementor plugin compatibility
	 *
	 * @param $widgets_id the widgets of the id
	 *
	 * @return mixed the widget classes of the id passed
	 *
	 * @since Eventium Decor 1.2.3
	 */
	function eventium_widget_class_names( $widgets_id ) {

		$widgets_id = str_replace( 'wp-widget-', '', $widgets_id );

		$classes = eventium_widgets_classes();

		$return_value = isset( $classes[ $widgets_id ] ) ? $classes[ $widgets_id ] : 'widget_featured_posts';

		return $return_value;
	}

endif;

if ( ! function_exists( 'eventium_widgets_classes' ) ) :

	/**
	 * Return all the arrays of widgets classes and classnames of same respectively
	 *
	 * @return array the array of widget classnames and its respective classes
	 *
	 * @since Eventium Decor 1.2.3
	 */
	function eventium_widgets_classes() {

		return
			array(
				'eventium_featured_posts_slider_widget'   => 'widget_featured_slider widget_featured_meta',
				'eventium_highlighted_posts_widget'       => 'widget_highlighted_posts widget_featured_meta',
				'eventium_featured_posts_widget'          => 'widget_featured_posts widget_featured_meta',
				'eventium_featured_posts_vertical_widget' => 'widget_featured_posts widget_featured_posts_vertical widget_featured_meta',
				'eventium_728x90_advertisement_widget'    => 'widget_300x250_advertisement',
				'eventium_300x250_advertisement_widget'   => 'widget_728x90_advertisement',
				'eventium_125x125_advertisement_widget'   => 'widget_125x125_advertisement',
			);
	}

endif;

/**
 * Load the Eventium Decor Elementor widgets file and registers it
 */
if ( ! function_exists( 'eventium_elementor_widgets_registered' ) ) :

	/**
	 * Load and register the required Elementor widgets file
	 *
	 * @param $widgets_manager
	 *
	 * @since Eventium Decor 1.2.3
	 */
	function eventium_elementor_widgets_registered( $widgets_manager ) {

		// Require the files
		// 1. Block Widgets
		
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/blog.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/blog-carousel.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/blog-grid.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/speakers.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/speaker.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/schedules.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/schedule-banner.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/tickets.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/ticket.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/speakers-list.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/countdown.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/statistic.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/clients.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/clients2.php';
		require EVENTIUM_ELEMENTOR_WIDGETS_DIR . '/gmap.php';

		// Register the widgets
		// 1. Block Widgets
		
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Blog() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Blog_Carousel() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Blog_Grid() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Speakers() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Speaker() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Schedules() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Shedule_Banner() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Tickets() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Ticket() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Speakers_List() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Countdown() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Statistic() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Clients() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Clients2() );
		$widgets_manager->register_widget_type( new \Elementor\Eventium_Elementor_Global_Widgets_Gmap() );
	}

endif;

add_action( 'elementor/widgets/widgets_registered', 'eventium_elementor_widgets_registered' );

if ( ! function_exists( 'eventium_elementor_category' ) ) :

	/**
	 * Add the Elementor category for use in Eventium Decor widgets as seperator
	 *
	 * @since Eventium Decor 1.2.3
	 */
	function eventium_elementor_category() {

		// Register widget block category for Elementor section
		\Elementor\Plugin::instance()->elements_manager->add_category( 'eventium-widget-blocks', array(
			'title' => esc_html__( 'Eventium Decor Widget Blocks', 'eventium' ),
		), 1 );

		// Register widget grid category for Elementor section
		\Elementor\Plugin::instance()->elements_manager->add_category( 'eventium-widget-grid', array(
			'title' => esc_html__( 'Eventium Decor Widget Grid', 'eventium' ),
		), 1 );

		// Register widget global category for Elementor section
		\Elementor\Plugin::instance()->elements_manager->add_category( 'eventium-widget-global', array(
			'title' => esc_html__( 'Eventium Decor Global Widgets', 'eventium' ),
		), 1 );
	}

endif;

add_action( 'elementor/init', 'eventium_elementor_category' );

