<?php
/**
 * Nunforest Elementor Global Widget Clients2.
 *
 * @package    ThemeGrill
 * @subpackage Nunforest
 * @since      Nunforest 1.2.3
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Clients2 extends Widget_Base {

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Clients2 widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'Eventium-Global-Widgets-Clients2';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Clients2 widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Eventium Clients2', 'eventium' );
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Clients2 widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Clients2 widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return array( 'eventium-widget-blocks' );
	}
	
	/**
	 * Register Eventium_Elementor_Global_Widgets_Clients2 widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__( 'Block Clients 2', 'eventium' ),
			)
		);

		$this->add_control(
			'client',
			[
				'label' => esc_html__( 'Clients2 List', 'eventium' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'client_title' => esc_html__( 'Clients2 #1', 'eventium' ),
						
					],
					[
						'client_title' => esc_html__( 'Clients2 #2', 'eventium' ),
						
					],
				],
				'fields' => [

					[
						'name' => 'client_title',
						'label' => esc_html__( 'Title', 'eventium' ),
						'type' => Controls_Manager::TEXT,
						'default' => esc_html__( 'Company Name' , 'eventium' ),
						'label_block' => true,
					],[
						'name' => 'client_subtitle',
						'label' => esc_html__( 'Subtitle', 'eventium' ),
						'type' => Controls_Manager::TEXT,
						'default' => esc_html__( 'Subtitle' , 'eventium' ),
						'label_block' => true,
					],
					[
						'name' => 'client_logo',
						'label' => esc_html__( 'Logo', 'eventium' ),
						'type' => Controls_Manager::MEDIA,
						'default' => '',
						'label_block' => true,
					],[
						'name' => 'client_link',
						'label' => esc_html__( 'Link', 'eventium' ),
						'type' => Controls_Manager::TEXT,
						'default' => '#',
						'label_block' => true,
					],[
						'name' => 'client_about',
						'label' => esc_html__( 'About Your Client', 'eventium' ),
						'type' => Controls_Manager::TEXTAREA,
						'default' => '',
						'label_block' => true,
					],
					
				],
				'title_field' => '{{{ client_title }}}',
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Clients2 widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render() {
		$client = $this->get_settings( 'client' );
		if ( $client ) {
		?>

		<div class="client-box">
		<ul class="client-list">
			<?php  foreach ( $client as $item ) { ?>
			<li>
				<a target="_blank" href="<?php echo esc_url($item['client_link']); ?>"><img src="<?php echo esc_url($item['client_logo']['url']); ?>" alt="<?php echo esc_attr($item['client_title']); ?>"></a>
				<div class="client-cont">
					<h2 class="heading2"><?php echo esc_html($item['client_title']); ?></h2>
					<span><?php echo esc_html($item['client_subtitle']); ?></span>
					<p><?php echo esc_html($item['client_about']); ?></p>
				</div>
			</li>
			<?php } ?>

		</ul>
	</div>

	
	<?php
		}
	}
}
