<?php

/**
 * Eventium Elementor Global Widget Title.
 *
 * @package    Nunforest
 * @subpackage Eventium
 * @since      Eventium  1.0
 */

namespace Elementor;

if (!defined('ABSPATH')) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Schedules extends Widget_Base
{

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'Eventium-Global-Widgets-Schedules';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return esc_html__('Eventium Schedules', 'eventium');
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Title widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return array('eventium-widget-blocks');
	}

	/**
	 * Register Eventium_Elementor_Global_Widgets_Title widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__('Schedules', 'eventium'),
			)
		);

		$this->add_control(
			'widget_number',
			array(
				'label'       => esc_html__('Number of Posts:', 'eventium'),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__('Add your number of posts', 'eventium'),
				'label_block' => true,
				'default'     => 6
			)
		);
		$this->add_control(
			'order',
			array(
				'label'       => esc_html__('Sort Posts:', 'eventium'),
				'type'        => Controls_Manager::SELECT,
				'placeholder' => esc_html__('Sort posts by', 'eventium'),
				'default' => 'default',
				'options' => array(
					'default'     => esc_html__('Default', 'eventium'),
					'ASC'     => esc_html__('ASC', 'eventium'),
					'DESC' => esc_html__('DESC', 'eventium'),
				),
			)
		);

		$this->add_control(
			'offset_posts_number',
			array(
				'label' => esc_html__('Offset Posts:', 'eventium'),
				'type'  => Controls_Manager::TEXT,

			)
		);


		$this->add_control(
			'show_apply',
			[
				'label' => esc_html__('Show Register Link', 'eventium'),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'label_on' => esc_html__('Show', 'eventium'),
				'label_off' => esc_html__('Hide', 'eventium'),
				'return_value' => 'yes',
			]
		);

		$this->add_control(
			'apply_link',
			array(
				'label' => esc_html__('Register Link:', 'eventium'),
				'type'  => Controls_Manager::TEXT,
				'default' => '#',
				'condition' => array(
					'show_apply' => 'yes',
				),

			)
		);
		$this->add_control(
			'apply_text',
			array(
				'label' => esc_html__('Resgister Text:', 'eventium'),
				'type'  => Controls_Manager::TEXT,
				'default' => esc_html__('Register now and buy ticket', 'eventium'),
				'condition' => array(
					'show_apply' => 'yes',
				),

			)
		);

		$this->add_control(
			'blog_type',
			array(
				'label'   => esc_html__('Schedules Layout Style:', 'eventium'),
				'type'    => Controls_Manager::SELECT,
				'default' => '1',
				'options' => array(
					'1' => esc_attr__('Tab Style', 'eventium'),
					'2' => esc_attr__('Accordion Style', 'eventium'),
					'3' => esc_attr__('Column Style', 'eventium'),
				),
			)
		);




		$this->end_controls_section();
	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Title widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render()
	{
		$posts_number        = $this->get_settings('widget_number');
		$order        = $this->get_settings('order');
		$offset_posts_number = $this->get_settings('offset_posts_number');
		$blog_type = $this->get_settings('blog_type');
		$show_apply = $this->get_settings('show_apply');
		$apply_link = $this->get_settings('apply_link');
		$apply_text = $this->get_settings('apply_text');
		$thumbnai_size = $this->get_settings('thumbnai_size');

		if (get_query_var('paged')) {
			$paged = get_query_var('paged');
		} elseif (get_query_var('page')) {
			$paged = get_query_var('page');
		} else {
			$paged = 1;
		}

		$args = array(
			'posts_per_page'      => $posts_number,
			'post_type'           => 'schedule',
			'ignore_sticky_posts' => true,

		);



		// Offset the posts
		if (!empty($offset_posts_number)) {
			$args['offset'] = $offset_posts_number;
		}

		if ($order != 'default') {
			$args['order'] = $order;
		}


		$query = new \WP_Query($args); ?>

		<?php if ($blog_type == '2') { ?>

			<div class="schedule-box">

				<div class="collapse-box">
					<div class="panel-group" id="accordion">

						<?php $portfolio_skills = get_terms('schedule_category'); ?>
						<?php $s = 1;
						foreach ($portfolio_skills as $portfolio_skill) { ?>

							<div class="panel panel-default">

								<div class="panel-heading" id="heading<?php echo esc_attr($portfolio_skill->slug); ?>">
									<h4 class="panel-title">
										<a <?php if ($s > 1) { ?>class="collapsed" <?php } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo esc_attr($portfolio_skill->slug); ?>" aria-expanded="<?php if ($s == 1) { ?>true<?php } else { ?>false<?php } ?>" aria-controls="collapse<?php echo esc_attr($portfolio_skill->slug); ?>">
											<?php esc_html_e('Dia', 'eventium'); ?> <?php echo esc_html($s); ?> / <?php echo esc_attr($portfolio_skill->name); ?>
										</a>
									</h4>
								</div>

								<div id="collapse<?php echo esc_attr($portfolio_skill->slug); ?>" data-parent="#accordion" class="panel-collapse collapse<?php if ($s == 1) { ?> show<?php } ?>" role="tabpanel" aria-labelledby="heading<?php echo esc_attr($portfolio_skill->slug); ?>">
									<div class="panel-body">
										<ul class="schedule-list">
											<?php
											$args = array(
												'posts_per_page'      => $posts_number,
												'post_type'           => 'schedule',
												'ignore_sticky_posts' => true,
												'tax_query' => array(
													array(
														'taxonomy' => 'schedule_category',
														'field'    => 'slug',
														'terms'    => $portfolio_skill->slug,
													),
												)

											);
											// Offset the posts
											if (!empty($offset_posts_number)) {
												$args['offset'] = $offset_posts_number;
											}
											if ($order != 'default') {
												$args['order'] = $order;
											}
											$portfolio = new \WP_Query($args);
											?>
											<?php $i = 1;
											while ($portfolio->have_posts()) : $portfolio->the_post(); ?>

												<?php
												$speaker_id = get_post_meta(get_the_ID(), '_eventium_speaker', true);
												?>
												<?php if (get_post_meta(get_the_ID(), '_eventium_lunch', 1)) {  ?>
													<li class="time-off-item">
														<i class="ionicons ion-coffee"></i>
														<span class="time"><?php echo esc_html(get_post_meta(get_the_ID(), '_eventium_time', true)); ?></span>

														<span class="time-off"><?php the_title(); ?></span>
													</li>
												<?php } elseif (get_post_meta(get_the_ID(), '_eventium_after', 1)) {  ?>
													<li class="time-off-item">
														<i class="ionicons ion-beer"></i>
														<span class="time"><?php echo esc_html(get_post_meta(get_the_ID(), '_eventium_time', true)); ?></span>

														<span class="time-off"><?php the_title(); ?></span>
													</li>
												<?php } else {  ?>
													<li>

														<?php echo get_the_post_thumbnail($speaker_id, 'thumbnail'); ?>
														<div class="schedule-cont">
															<span class="time"><?php echo esc_html(get_post_meta(get_the_ID(), '_eventium_time', true)); ?></span>
															<p><?php the_title(); ?></p>
															<p class="schedule-auth"><?php esc_html_e('by', 'eventium'); ?> <span><?php echo get_the_title($speaker_id); ?></span></p>
														</div>
													</li>

												<?php } ?>
											<?php $i++;
											endwhile;
											wp_reset_postdata(); ?>

										</ul>
									</div>
								</div>

							</div>

						<?php $s++;
						} ?>
					</div>

				</div>
			</div>

			<?php wp_reset_postdata(); ?>

		<?php } elseif ($blog_type == '3') { ?>

			<div class="row">
				<?php $portfolio_skills = get_terms('schedule_category'); ?>
				<?php $t = 1;
				foreach ($portfolio_skills as $portfolio_skill) { ?>

					<?php
					$args = array(
						'posts_per_page'      => $posts_number,
						'post_type'           => 'schedule',
						'ignore_sticky_posts' => true,
						'tax_query' => array(
							array(
								'taxonomy' => 'schedule_category',
								'field'    => 'slug',
								'terms'    => $portfolio_skill->slug,
							),
						)

					);
					// Offset the posts
					if (!empty($offset_posts_number)) {
						$args['offset'] = $offset_posts_number;
					}
					if ($order != 'default') {
						$args['order'] = $order;
					}
					$portfolio = new \WP_Query($args);
					?>

					<div class="col-lg-4">
						<div class="schedule-block">
							<h2 class="heading2"><?php echo esc_attr($portfolio_skill->name); ?></h2>
							<ul class="schedule-list">
								<?php $i = 1;
								while ($portfolio->have_posts()) : $portfolio->the_post(); ?>

									<?php
									$speaker_id = get_post_meta(get_the_ID(), '_eventium_speaker', true);
									?>
									<?php if (get_post_meta(get_the_ID(), '_eventium_lunch', 1)) {  ?>
										<li class="time-off-item">
											<span class="time"><?php echo esc_html(get_post_meta(get_the_ID(), '_eventium_time', true)); ?></span>
											<i class="ionicons ion-coffee"></i>
											<span class="time-off"><?php the_title(); ?></span>
										</li>
									<?php } elseif (get_post_meta(get_the_ID(), '_eventium_after', 1)) {  ?>
										<li class="time-off-item">
											<span class="time"><?php echo esc_html(get_post_meta(get_the_ID(), '_eventium_time', true)); ?></span>
											<i class="ionicons ion-beer"></i>
											<span class="time-off"><?php the_title(); ?></span>
										</li>
									<?php } else {  ?>
										<li>
											<span class="time"><?php echo esc_html(get_post_meta(get_the_ID(), '_eventium_time', true)); ?></span>
											<?php echo get_the_post_thumbnail($speaker_id, 'thumbnail'); ?>
											<div class="schedule-cont">
												<p><?php the_title(); ?></p>
												<p class="schedule-auth"><?php esc_html_e('by', 'eventium'); ?> <span><?php echo get_the_title($speaker_id); ?></span></p>
											</div>
										</li>

									<?php } ?>
								<?php $i++;
								endwhile;
								wp_reset_postdata(); ?>
							</ul>
						</div>
					</div>
				<?php } ?>
			</div>
			<?php wp_reset_postdata(); ?>

		<?php } else { ?>

			<div class="schedule-box">


				<div class="schedule-tabs">

					<nav class="nav nav-tabs" id="myTab" role="tablist">
						<?php $portfolio_skills = get_terms('schedule_category'); ?>
						<?php $s = 1;
						foreach ($portfolio_skills as $portfolio_skill) { ?>
							<a class="nav-item nav-link<?php if ($s == 1) { ?> active<?php } ?>" id="nav-<?php echo esc_attr($portfolio_skill->slug); ?>-tab" data-toggle="tab" href="#nav-<?php echo esc_attr($portfolio_skill->slug); ?>" role="tab" aria-controls="nav-<?php echo esc_attr($portfolio_skill->slug); ?>" <?php if ($s == 1) { ?>aria-selected="true" <?php } ?>><?php esc_html_e('Dia', 'eventium'); ?> <?php echo esc_html($s); ?> / <?php echo esc_attr($portfolio_skill->name); ?></a>
						<?php $s++;
						} ?>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<?php $t = 1;
						foreach ($portfolio_skills as $portfolio_skill) { ?>
							<div class="tab-pane fade<?php if ($t == 1) { ?> show active<?php } ?>" id="nav-<?php echo esc_attr($portfolio_skill->slug); ?>" role="tabpanel" aria-labelledby="nav-<?php echo esc_attr($portfolio_skill->slug); ?>-tab">
								<?php
								$args = array(
									'posts_per_page'      => $posts_number,
									'post_type'           => 'schedule',
									'ignore_sticky_posts' => true,
									'tax_query' => array(
										array(
											'taxonomy' => 'schedule_category',
											'field'    => 'slug',
											'terms'    => $portfolio_skill->slug,
										),
									)

								);
								// Offset the posts
								if (!empty($offset_posts_number)) {
									$args['offset'] = $offset_posts_number;
								}
								if ($order != 'default') {
									$args['order'] = $order;
								}
								$portfolio = new \WP_Query($args);
								?>
								<?php $i = 1;
								while ($portfolio->have_posts()) : $portfolio->the_post(); ?>
									<?php if ($i % 2 == 1 or $i == 1) { ?>
										<div class="row">
										<?php } ?>
										<?php if (get_post_meta(get_the_ID(), '_eventium_lunch', 1)) {  ?>
											<div class="col-lg-6">
												<div class="schedule-post time-off-item">
													<span>
														<img src="<?php echo get_template_directory_uri() ?>/images/icons/meal.png" alt="meal">
													</span>
													<div class="schedule-cont">
														<span class="time"><?php echo esc_html(get_post_meta(get_the_ID(), '_eventium_time', true)); ?></span>
														<!-- <span class="time-off"><?php the_title(); ?></span> -->
														<p class="schedule-title"><?php the_title(); ?></p>
														<?php the_excerpt(); ?>
													</div>
												</div>
											</div>
										<?php } elseif (get_post_meta(get_the_ID(), '_eventium_nelore', 1)) {  ?>
											<div class="col-lg-6">
												<div class="schedule-post time-off-item">
													<span>
														<img src="<?php echo get_template_directory_uri() ?>/images/icons/map-marker-white.png" alt="meal">
													</span>
													<div class="schedule-cont">
														<span class="time"><?php echo esc_html(get_post_meta(get_the_ID(), '_eventium_time', true)); ?></span>
														<!-- <span class="time-off"><?php the_title(); ?></span> -->
														<p class="schedule-title"><?php the_title(); ?></p>
														<?php the_excerpt(); ?>
													</div>
												</div>
											</div>
										<?php } else { ?>
											<div class="col-lg-6">
												<div class="schedule-post">
													<?php
													$speaker_id = get_post_meta(get_the_ID(), '_eventium_speaker', true);
													?>
													<?php echo get_the_post_thumbnail($speaker_id, 'thumbnail'); ?>
													<div class="schedule-cont">
														<span class="time"><?php echo esc_html(get_post_meta(get_the_ID(), '_eventium_time', true)); ?></span>
														<p class="schedule-title"><?php the_title(); ?></p>
														<?php the_excerpt(); ?>
														<!-- <p class="schedule-auth"><?php esc_html_e('by', 'eventium'); ?> <span><?php echo get_the_title($speaker_id); ?></span></p> -->
													</div>
												</div>
											</div>
										<?php } ?>
										<?php if ($i % 2 == 0 or $i == $portfolio->post_count) { ?>
										</div>
									<?php } ?>
								<?php $i++;
								endwhile;
								wp_reset_postdata(); ?>

							</div>
						<?php $t++;
						} ?>
						<?php if ($show_apply == 'yes') { ?>
							<div class="center-area">
								<a class="default-button" href="<?php echo esc_url($apply_link); ?>"><?php echo esc_html($apply_text); ?></a>
							</div>
						<?php } ?>
					</div>

				</div>
			</div>

		<?php } ?>


<?php

	}
}
