<?php

/**
 * Eventium Elementor Global Widget Title.
 *
 * @package    Nunforest
 * @subpackage Eventium
 * @since      Eventium  1.0
 */

namespace Elementor;

if (!defined('ABSPATH')) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Ticket extends Widget_Base
{

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'Eventium-Global-Widgets-Ticket';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return esc_html__('Eventium Ticket', 'eventium');
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Title widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return array('eventium-widget-blocks');
	}

	/**
	 * Register Eventium_Elementor_Global_Widgets_Title widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__('Ticket', 'eventium'),
			)
		);

		$this->add_control(
			'ticket_selected',
			array(
				'label'     => esc_html__('Select Speaker:', 'eventium'),
				'type'      => Controls_Manager::SELECT,
				'options'   => eventium_get_ticket_post_options(),
			)
		);


		$this->add_control(
			'blog_type',
			array(
				'label'   => esc_html__('Ticket Layout Style:', 'eventium'),
				'type'    => Controls_Manager::SELECT,
				'default' => '1',
				'options' => array(
					'1' => esc_attr__('Style 1', 'eventium'),
					'2' => esc_attr__('Style 2', 'eventium'),
					'3' => esc_attr__('Style 3', 'eventium'),
				),
			)
		);




		$this->end_controls_section();
	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Title widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render()
	{
		$posts_number        = 1;
		$blog_type = $this->get_settings('blog_type');
		$ticket_selected = $this->get_settings('ticket_selected');

		if (get_query_var('paged')) {
			$paged = get_query_var('paged');
		} elseif (get_query_var('page')) {
			$paged = get_query_var('page');
		} else {
			$paged = 1;
		}

		$args = array(
			'posts_per_page'      => $posts_number,
			'post_type'           => 'pricing',
			'p'                   => $ticket_selected,
			'ignore_sticky_posts' => true,

		);



		$query = new \WP_Query($args); ?>

		<?php if ($blog_type == '2') { ?>


			<?php $i = 1;
			while ($query->have_posts()) : $query->the_post(); ?>
				<?php
				$item_cats = get_the_terms(get_the_ID(), 'pricing_category');
				$item_skill = '';
				$j = 1;
				foreach ((array)$item_cats as $item_cat) {
					if ($j = 1) {
						$item_skill .= $item_cat->name;
					} else {
						$item_skill .= ', ' . $item_cat->name;
					}
					$j++;
				}

				?>



				<div class="table-box">
					<div class="price-tab">
						<div class="inner-price-tab">
							<h2 class="heading2"><?php the_title(); ?></h2>
							<p><span>USD<?php echo esc_html(get_post_meta(get_the_ID(), '_price', true)); ?></span> ?></p>
							<!-- <p><span>USD<?php echo esc_html(get_post_meta(get_the_ID(), '_price', true)); ?></span>/ <?php echo esc_html($item_skill); ?></p> -->
							<a href="#" class="default-button"><?php esc_html_e('Comprar', 'eventium'); ?></a>
						</div>
					</div>
					<div class="list-tab">
						<?php $list = get_post_meta(get_the_ID(), '_eventium_list', true); ?>
						<?php if (count($list) > 0) { ?>
							<ul class="pricing-list">
								<?php foreach ((array) $list as $key => $item) { ?>
									<li><?php echo esc_html($item['_eventium_litem']); ?></li>
								<?php } ?>
							</ul>
						<?php } ?>
					</div>
				</div>





			<?php $i++;
			endwhile; ?>


			<?php wp_reset_postdata(); ?>

		<?php } elseif ($blog_type == '3') { ?>


			<?php wp_reset_postdata(); ?>

		<?php } else { ?>


			<?php $i = 1;
			while ($query->have_posts()) : $query->the_post(); ?>
				<?php
				$item_cats = get_the_terms(get_the_ID(), 'pricing_category');
				$item_skill = '';
				$j = 1;
				foreach ((array)$item_cats as $item_cat) {
					if ($j = 1) {
						$item_skill .= $item_cat->name;
					} else {
						$item_skill .= ', ' . $item_cat->name;
					}
					$j++;
				}

				?>

				<div class="table-box<?php if (get_post_meta(get_the_ID(), '_eventium_enterprise', 1)) {  ?> enterprise<?php } ?>">
					<h2 class="heading2"><?php the_title(); ?></h2>
					<!-- <p><span>USD<?php echo esc_html(get_post_meta(get_the_ID(), '_price', true)); ?></span>/ <?php echo esc_html($item_skill); ?></p> -->
					<p><span>USD<?php echo esc_html(get_post_meta(get_the_ID(), '_price', true)); ?></span>?></p>
					<?php $list = get_post_meta(get_the_ID(), '_eventium_list', true); ?>
					<?php if (count($list) > 0) { ?>
						<ul class="pricing-list">
							<?php foreach ((array) $list as $key => $item) { ?>
								<li><?php echo esc_html($item['_eventium_litem']); ?></li>
							<?php } ?>
						</ul>
					<?php } ?>
					<a href="#" class="default-button"><?php esc_html_e('Comprar', 'eventium'); ?></a>
				</div>


			<?php $i++;
			endwhile; ?>

			<?php wp_reset_postdata(); ?>



		<?php } ?>


<?php

	}
}
