<?php
/**
 * Constrix Elementor Global Widget Statistic.
 *
 * @package    ThemeGrill
 * @subpackage Constrix
 * @since      Constrix 1.2.3
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Statistic extends Widget_Base {

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Statistic widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'Eventium-Global-Widgets-Statistic';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Statistic widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Eventium Statistic', 'eventium' );
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Statistic widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Statistic widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return array( 'eventium-widget-blocks' );
	}
	
	/**
	 * Register Eventium_Elementor_Global_Widgets_Statistic widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__( 'Block Statistic', 'eventium' ),
			)
		);
		
		

		$this->add_control(
			'widget_title',
			array(
				'label'       => esc_html__( 'Title:', 'eventium' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Add your custom block title', 'eventium' ),
				'default' => '',
				'label_block' => true,
			)
		);

		$this->add_control(
			'widget_title2',
			array(
				'label'       => esc_html__( 'Number:', 'eventium' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Add your counter number', 'eventium' ),
				'default' => '',
				'label_block' => true,
			)
		);


		
		$this->add_control(
			'count_date',
			array(
				'label'       => esc_html__( 'After number:', 'eventium' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Add your after number letter', 'eventium' ),
				'default' => '',
				'label_block' => true,
			)
		);
		


		$this->end_controls_section();

	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Statistic widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render() {
		$widget_title = $this->get_settings( 'widget_title' );
		$widget_title2 = $this->get_settings( 'widget_title2' );
		$count_date = $this->get_settings( 'count_date' );
		?>

		<div class="statistic-post">
		    <h2 class="heading2"><?php echo esc_html($widget_title); ?></h2>
		    <p class="timer-elem"><span class="timer" data-from="0" data-to="<?php echo esc_attr($widget_title2); ?>"><?php echo esc_html($widget_title2); ?></span><?php echo esc_html($count_date); ?></p>
		</div>

<?php 
}
}
?>