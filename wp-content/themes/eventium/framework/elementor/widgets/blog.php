<?php

/**
 * Eventium Elementor Global Widget Title.
 *
 * @package    Nunforest
 * @subpackage Eventium
 * @since      Eventium  1.0
 */

namespace Elementor;

if (!defined('ABSPATH')) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Blog extends Widget_Base
{

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'Eventium-Global-Widgets-Blog';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return esc_html__('Eventium Blog', 'eventium');
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Title widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return array('eventium-widget-blocks');
	}

	/**
	 * Register Eventium_Elementor_Global_Widgets_Title widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__('Blog Posts', 'eventium'),
			)
		);

		$this->add_control(
			'widget_number',
			array(
				'label'       => esc_html__('Number of Posts:', 'eventium'),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__('Add your number of posts', 'eventium'),
				'label_block' => true,
				'default'     => 6
			)
		);
		$this->add_control(
			'order',
			array(
				'label'       => esc_html__('Sort Posts:', 'eventium'),
				'type'        => Controls_Manager::SELECT,
				'placeholder' => esc_html__('Sort posts by', 'eventium'),
				'default' => 'default',
				'options' => array(
					'default'     => esc_html__('Default', 'eventium'),
					'ASC'     => esc_html__('ASC', 'eventium'),
					'DESC' => esc_html__('DESC', 'eventium'),
				),
			)
		);

		$this->add_control(
			'offset_posts_number',
			array(
				'label' => esc_html__('Offset Posts:', 'eventium'),
				'type'  => Controls_Manager::TEXT,

			)
		);

		$this->add_control(
			'display_type',
			array(
				'label'   => esc_html__('Display the posts from:', 'eventium'),
				'type'    => Controls_Manager::SELECT,
				'default' => 'latest',
				'options' => array(
					'latest'     => esc_html__('Latest Posts', 'eventium'),
					'categories' => esc_html__('Categories', 'eventium'),
				),
			)
		);

		$this->add_control(
			'categories_selected',
			array(
				'label'     => esc_html__('Select categories:', 'eventium'),
				'type'      => Controls_Manager::SELECT,
				'options'   => eventium_elementor_categories(),
				'condition' => array(
					'display_type' => 'categories',
				),
			)
		);

		$this->add_control(
			'category_not_in',
			array(
				'label'     => esc_html__('Exclude Category:', 'eventium'),
				'type'      => Controls_Manager::SELECT,
				'options'   => eventium_elementor_categories(),

			)
		);

		$this->add_control(
			'blog_type',
			array(
				'label'   => esc_html__('Blog Layout Style:', 'eventium'),
				'type'    => Controls_Manager::SELECT,
				'default' => '1',
				'options' => array(
					'1' => esc_attr__('Blog List', 'eventium'),
					'2' => esc_attr__('Grid 2 Column', 'eventium'),
					'3' => esc_attr__('Grid 3 Coulmn', 'eventium'),
				),
			)
		);

		$this->add_control(
			'show_meta',
			[
				'label' => esc_html__('Show Post Meta', 'eventium'),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_on' => esc_html__('Show', 'eventium'),
				'label_off' => esc_html__('Hide', 'eventium'),
				'return_value' => 'yes',
			]
		);
		$this->add_control(
			'show_excerpt',
			[
				'label' => esc_html__('Show Post Excerpt', 'eventium'),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_on' => esc_html__('Show', 'eventium'),
				'label_off' => esc_html__('Hide', 'eventium'),
				'return_value' => 'yes',
			]
		);

		$this->add_control(
			'show_pagination',
			[
				'label' => esc_html__('Show Pagination', 'eventium'),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_on' => esc_html__('Show', 'eventium'),
				'label_off' => esc_html__('Hide', 'eventium'),
				'return_value' => 'yes',
			]
		);


		$this->end_controls_section();
	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Title widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render()
	{
		$posts_number        = $this->get_settings('widget_number');
		$display_type        = $this->get_settings('display_type');
		$order        = $this->get_settings('order');
		$offset_posts_number = $this->get_settings('offset_posts_number');
		$categories_selected = $this->get_settings('categories_selected');
		$category_not_in = $this->get_settings('category_not_in');
		$blog_type = $this->get_settings('blog_type');
		$show_meta = $this->get_settings('show_meta');
		$show_pagination = $this->get_settings('show_pagination');
		$show_excerpt = $this->get_settings('show_excerpt');

		if (get_query_var('paged')) {
			$paged = get_query_var('paged');
		} elseif (get_query_var('page')) {
			$paged = get_query_var('page');
		} else {
			$paged = 1;
		}

		$args = array(
			'posts_per_page'      => $posts_number,
			'post_type'           => 'post',
			'ignore_sticky_posts' => true,

		);

		// Display from the category selected
		if ('categories' == $display_type) {
			$args['category__in'] = $categories_selected;
		}

		// Offset the posts
		if (!empty($offset_posts_number)) {
			$args['offset'] = $offset_posts_number;
		}
		if (!empty($category_not_in)) {
			$args['cat'] = '-' . $category_not_in;
		}

		if ($order != 'default') {
			$args['order'] = $order;
		}

		$pagi = $this->get_settings('show_pagination');
		if ($pagi == 'yes') {
			$args['paged'] = $paged;
		}

		$query = new \WP_Query($args); ?>

		<?php if ($blog_type == '2') { ?>

			<div class="blog-box iso-call with-sidebar">
				<?php $i = 1;
				while ($query->have_posts()) : $query->the_post(); ?>

					<?php get_template_part('grid', (post_type_supports(get_post_type(), 'post-formats') ? get_post_format() : get_post_type())); ?>

				<?php $i++;
				endwhile; ?>
			</div>

			<?php if ($show_pagination == 'yes') { ?>
				<div class="pagination-box">
					<?php eventium_pagination($prev = esc_html__('prev', 'eventium'), $next = esc_html__('next', 'eventium'), $pages = $query->max_num_pages); ?>

				</div>
			<?php } ?>
			<?php wp_reset_postdata(); ?>

		<?php } elseif ($blog_type == '3') { ?>

			<div class="blog-box iso-call">
				<?php $i = 1;
				while ($query->have_posts()) : $query->the_post(); ?>

					<?php get_template_part('grid3', (post_type_supports(get_post_type(), 'post-formats') ? get_post_format() : get_post_type())); ?>

				<?php $i++;
				endwhile; ?>
			</div>

			<?php if ($show_pagination == 'yes') { ?>
				<div class="pagination-box">
					<?php eventium_pagination($prev = esc_html__('prev', 'eventium'), $next = esc_html__('next', 'eventium'), $pages = $query->max_num_pages); ?>

				</div>
			<?php } ?>
			<?php wp_reset_postdata(); ?>

		<?php } else { ?>

			<div class="blog-box">

				<?php $i = 1;
				while ($query->have_posts()) : $query->the_post(); ?>

					<div <?php post_class('blog-post'); ?>>

						<?php the_post_thumbnail(); ?>
						<?php if ($show_meta == 'yes') { ?>
							<span class="date-comments"><?php the_time(get_option('date_format')); ?>, <?php
																											comments_popup_link(esc_html__('0 Comments', 'eventium'), esc_html__('1 Comment', 'eventium'), esc_html__('% Comments', 'eventium'), '',  esc_html__('Comment off', 'eventium'));
																											?></span>
						<?php } ?>
						<h2 class="heading2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php if ($show_excerpt == 'yes') { ?>
							<?php the_excerpt(); ?>
						<?php } ?>
						<div class="row">
							<div class="col-md-6">
								<a href="<?php the_permalink(); ?>" class="continue-read"><?php esc_html_e('Seguir leyendo', 'eventium'); ?> <i class="fa fa-angle-right"></i></a>
							</div>
							<div class="col-md-6">
								<ul class="post-tags">
									<li>
										<?php esc_html_e('in', 'eventium'); ?>
									</li>
									<li>
										<?php the_category(', '); ?>
									</li>
								</ul>
							</div>
						</div>


					</div>



				<?php $i++;
				endwhile; ?>
				<?php if ($show_pagination == 'yes') { ?>
					<div class="pagination-box">
						<?php eventium_pagination($prev = esc_html__('prev', 'eventium'), $next = esc_html__('next', 'eventium'), $pages = $query->max_num_pages); ?>

					</div>
				<?php } ?>
				<?php wp_reset_postdata(); ?>
			</div>

		<?php } ?>


<?php

	}
}
