<?php

/**
 * Constrix Elementor Global Widget Title.
 *
 * @package    Aucreative
 * @subpackage Arch Decor
 * @since      Arc Decor 1.0
 */

namespace Elementor;

if (!defined('ABSPATH')) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Blog_Grid extends Widget_Base
{

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'Eventium-Global-Widgets-Blog-Grid';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return esc_html__('Eventium Blog Grid', 'eventium');
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Title widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return array('eventium-widget-blocks');
	}

	/**
	 * Register Eventium_Elementor_Global_Widgets_Title widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__('Blog Grid', 'eventium'),
			)
		);

		$this->add_control(
			'widget_number',
			array(
				'label'       => esc_html__('Number of Posts:', 'eventium'),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__('Add your number of posts', 'eventium'),
				'label_block' => true,
				'default'     => 6
			)
		);

		$this->add_control(
			'offset_posts_number',
			array(
				'label' => esc_html__('Offset Posts:', 'eventium'),
				'type'  => Controls_Manager::TEXT,

			)
		);

		$this->add_control(
			'order',
			array(
				'label'       => esc_html__('Sort Posts:', 'eventium'),
				'type'        => Controls_Manager::SELECT,
				'placeholder' => esc_html__('Sort posts by', 'eventium'),
				'default' => 'default',
				'options' => array(
					'default'     => esc_html__('Default', 'eventium'),
					'ASC'     => esc_html__('ASC', 'eventium'),
					'DESC' => esc_html__('DESC', 'eventium'),
				),
			)
		);



		$this->add_control(
			'display_type',
			array(
				'label'   => esc_html__('Display the posts from:', 'eventium'),
				'type'    => Controls_Manager::SELECT,
				'default' => 'latest',
				'options' => array(
					'latest'     => esc_html__('Latest Posts', 'eventium'),
					'categories' => esc_html__('Categories', 'eventium'),
				),
			)
		);



		$this->add_control(
			'categories_selected',
			array(
				'label'     => esc_html__('Select categories:', 'eventium'),
				'type'      => Controls_Manager::SELECT,
				'options'   => eventium_elementor_categories(),
				'condition' => array(
					'display_type' => 'categories',
				),
			)
		);






		$this->end_controls_section();
	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Title widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render()
	{
		$posts_number        = $this->get_settings('widget_number');
		$display_type        = $this->get_settings('display_type');
		$offset_posts_number = $this->get_settings('offset_posts_number');
		$categories_selected = $this->get_settings('categories_selected');
		$order        = $this->get_settings('order');
		if (get_query_var('paged')) {
			$paged = get_query_var('paged');
		} elseif (get_query_var('page')) {
			$paged = get_query_var('page');
		} else {
			$paged = 1;
		}

		$args = array(
			'posts_per_page'      => $posts_number,
			'post_type'           => 'post',
			'ignore_sticky_posts' => true,

		);

		// Display from the category selected
		if ('categories' == $display_type) {
			$args['category__in'] = $categories_selected;
		}

		// Offset the posts
		if (!empty($offset_posts_number)) {
			$args['offset'] = $offset_posts_number;
		}

		if ($order != 'default') {
			$args['order'] = $order;
		}

		$query = new \WP_Query($args); ?>

		<div class="news-box">
			<?php $i = 1;
			while ($query->have_posts()) : $query->the_post(); ?>

				<?php if ($i % 3 == 1 or $i == 1) { ?>
					<div class="row">
					<?php } ?>

					<div class="col-md-4">
						<div class="blog-post">
							<?php the_post_thumbnail(); ?>
							<span class="date-comments"><?php the_time(get_option('date_format')); ?></span>
							<h2 class="heading2"><a href="<?php the_permalink(); ?>"><?php the_title('eventium-grid'); ?></a></h2>
							<p><?php echo eventium_excerpt($limit = 12); ?></p>
							<a href="<?php the_permalink(); ?>" class="continue-read"><?php esc_html_e('Seguir leyendo', 'eventium'); ?></a>
						</div>
					</div>

					<?php if ($i % 3 == 0 or $i == $query->post_count) { ?>
					</div>
				<?php } ?>

			<?php $i++;
			endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div>



<?php

	}
}
