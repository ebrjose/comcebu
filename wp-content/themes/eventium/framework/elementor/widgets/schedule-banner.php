<?php
/**
 * Constrix Elementor Global Widget Shedule_Banner.
 *
 * @package    ThemeGrill
 * @subpackage Constrix
 * @since      Constrix 1.2.3
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Shedule_Banner extends Widget_Base {

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Shedule_Banner widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'Eventium-Global-Widgets-Shedule_Banner';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Shedule_Banner widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Eventium Shedule_Banner', 'eventium' );
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Shedule_Banner widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Shedule_Banner widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return array( 'eventium-widget-blocks' );
	}
	
	/**
	 * Register Eventium_Elementor_Global_Widgets_Shedule_Banner widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__( 'Block Shedule_Banner', 'eventium' ),
			)
		);
		
		$this->add_control(
			'image',
			array(
				'label'       => esc_html__( 'Image:', 'eventium' ),
				'type'        => Controls_Manager::MEDIA,
				'placeholder' => esc_html__( 'Upload your service image', 'eventium' ),
				'label_block' => true,
			)
		);
		
		$this->add_control(
			'label',
			[
				'label' => esc_html__( 'Schedule label', 'eventium' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'sechedule_title' => esc_html__( 'Lunch time / 12:00-13:30', 'eventium' ),
						
					],
				],
				'fields' => [

					[
						'name' => 'sechedule_title',
						'label' => esc_html__( 'Sechedule Title', 'eventium' ),
						'type' => Controls_Manager::TEXT,
						'default' => esc_html__( 'Name' , 'eventium' ),
						'label_block' => true,
					],
					
				],
				'title_field' => '{{{ sechedule_title }}}',
			]
		);


		$this->end_controls_section();

	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Shedule_Banner widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render() {
		$image = $this->get_settings( 'image' );
		$label = $this->get_settings( 'label' );
		?>

		<div class="schedule-image">
			<img src="<?php echo esc_url($image['url']); ?>" alt="sh">
			<?php if ( $label ) { ?>
				<div class="hover-box">
					<?php  foreach ( $label as $item ) { ?>
						<div class="white-box">
							<p><?php echo esc_html($item['sechedule_title']); ?></p>
						</div>
					<?php } ?>
				</div>
			<?php }?>
		</div>

		
<?php 
}
}
?>