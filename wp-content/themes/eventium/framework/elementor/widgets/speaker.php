<?php
/**
 * Eventium Elementor Global Widget Title.
 *
 * @package    Nunforest
 * @subpackage Eventium 
 * @since      Eventium  1.0
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Speaker extends Widget_Base {

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'Eventium-Global-Widgets-Speaker';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Eventium Speaker', 'eventium' );
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Title widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return array( 'eventium-widget-blocks' );
	}
	
	/**
	 * Register Eventium_Elementor_Global_Widgets_Title widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__( 'Speaker', 'eventium' ),
			)
		);

		$this->add_control(
			'speaker_selected',
			array(
				'label'     => esc_html__( 'Select Speaker:', 'eventium' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => eventium_get_speaker_post_options(),
			)
		);


		$this->add_control(
			'thumbnai_size',
			array(
				'label'       => esc_html__( 'Thumbnail Size.  Example: eventium-team, large, thumbnail...:', 'eventium' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Add your thumbnail size.', 'eventium' ),
				'default'     => '',
			)
		);


		$this->add_control(
			'blog_type',
			array(
				'label'   => esc_html__( 'Speaker Layout Style:', 'eventium' ),
				'type'    => Controls_Manager::SELECT,
				'default' => '1',
				'options' => array(
					'1' => esc_attr__( 'Style 1', 'eventium' ),
					'2' => esc_attr__( 'Style 2', 'eventium' ),
				),
			)
		);

		


		$this->end_controls_section();
	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Title widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render() {
		$posts_number        = 1;
		$speaker_selected = $this->get_settings( 'speaker_selected' );
		$blog_type = $this->get_settings( 'blog_type' );
		$thumbnai_size = $this->get_settings( 'thumbnai_size' );

		 if ( get_query_var('paged') ) {
	    $paged = get_query_var('paged');
	    } elseif ( get_query_var('page') ) {
	        $paged = get_query_var('page');
	    } else {
	        $paged = 1;
	    }   

		$args = array(
			'posts_per_page'      => $posts_number,
			'post_type'           => 'speaker',
			'p'                   => $speaker_selected,
			'ignore_sticky_posts' => true,
			
		);

		
		
		$query = new \WP_Query($args); ?>

		<?php if($blog_type=='2'){ ?>
			<?php $i=1; while($query->have_posts()) : $query->the_post(); ?>

				
						<div class="speaker-post second-style">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail($thumbnai_size); ?></a>
							<div class="speaker-content">
								<h2 class="heading2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<?php $id = get_the_ID(); ?>
								<?php if(get_post_meta($id, '_eventium_job', true)!=''){ ?>
									<span><?php echo esc_html(get_post_meta($id, '_eventium_job', true)); ?></span>
								<?php } ?>
								<p><?php echo esc_html(eventium_excerpt($limit=16)); ?></p>
								<ul class="social-links">
								    <?php if(get_post_meta($id, '_eventium_facebook', true)!=''){ ?>
									<li><a class="facebook" href="<?php echo esc_url(get_post_meta($id, '_eventium_facebook', true)); ?>"><i class="fa fa-facebook"></i></a></li>
									<?php } ?>
									<?php if(get_post_meta($id, '_eventium_twitter', true)!=''){ ?>
									<li><a class="twitter" href="<?php echo esc_url(get_post_meta($id, '_eventium_twitter', true)); ?>"><i class="fa fa-twitter"></i></a></li>
									<?php } ?>
									<?php if(get_post_meta($id, '_eventium_linkedin', true)!=''){ ?>
									<li><a class="linkedin" href="<?php echo esc_url(get_post_meta($id, '_eventium_linkedin', true)); ?>"><i class="fa fa-linkedin"></i></a></li>
									<?php } ?>
									<?php if(get_post_meta($id, '_eventium_instagram', true)!=''){ ?>
									<li><a class="personal-web" href="<?php echo esc_url(get_post_meta($id, '_eventium_instagram', true)); ?>"><i class="fa fa-instagram"></i></a></li>
									<?php } ?>
									<?php if(get_post_meta($id, '_eventium_pinterest', true)!=''){ ?>
									<li><a class="personal-web" href="<?php echo esc_url(get_post_meta($id, '_eventium_pinterest', true)); ?>"><i class="fa fa-pinterest"></i></a></li>
									<?php } ?>
								</ul>
							</div>
						</div>
					
				
				<?php $i++; endwhile; ?>

			<?php wp_reset_postdata(); ?>

		<?php }elseif($blog_type=='3'){ ?>

			
			<?php wp_reset_postdata(); ?>

		<?php }else{ ?>


				<?php $i=1; while($query->have_posts()) : $query->the_post(); ?>

					

						<div class="speaker-post">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail($thumbnai_size); ?></a>
							<div class="speaker-content">
								<h2 class="heading2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<?php $id = get_the_ID(); ?>
								<?php if(get_post_meta($id, '_eventium_job', true)!=''){ ?>
									<span><?php echo esc_html(get_post_meta($id, '_eventium_job', true)); ?></span>
								<?php } ?>
								
									<ul class="social-links">
									    <?php if(get_post_meta($id, '_eventium_facebook', true)!=''){ ?>
										<li><a class="facebook" href="<?php echo esc_url(get_post_meta($id, '_eventium_facebook', true)); ?>"><i class="fa fa-facebook"></i></a></li>
										<?php } ?>
										<?php if(get_post_meta($id, '_eventium_twitter', true)!=''){ ?>
										<li><a class="twitter" href="<?php echo esc_url(get_post_meta($id, '_eventium_twitter', true)); ?>"><i class="fa fa-twitter"></i></a></li>
										<?php } ?>
										<?php if(get_post_meta($id, '_eventium_linkedin', true)!=''){ ?>
										<li><a class="linkedin" href="<?php echo esc_url(get_post_meta($id, '_eventium_linkedin', true)); ?>"><i class="fa fa-linkedin"></i></a></li>
										<?php } ?>
										<?php if(get_post_meta($id, '_eventium_instagram', true)!=''){ ?>
										<li><a class="personal-web" href="<?php echo esc_url(get_post_meta($id, '_eventium_instagram', true)); ?>"><i class="fa fa-instagram"></i></a></li>
										<?php } ?>
										<?php if(get_post_meta($id, '_eventium_pinterest', true)!=''){ ?>
										<li><a class="personal-web" href="<?php echo esc_url(get_post_meta($id, '_eventium_pinterest', true)); ?>"><i class="fa fa-pinterest"></i></a></li>
										<?php } ?>
									</ul>
								
							</div>
						</div>
					
				<?php $i++; endwhile; ?>
				
				<?php wp_reset_postdata(); ?>

		<?php } ?>

		 
		<?php 

	}
}
