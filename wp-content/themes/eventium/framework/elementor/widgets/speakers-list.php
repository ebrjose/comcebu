<?php
/**
 * Constrix Elementor Global Widget Speakers_List.
 *
 * @package    ThemeGrill
 * @subpackage Constrix
 * @since      Constrix 1.2.3
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Speakers_List extends Widget_Base {

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Speakers_List widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'Eventium-Global-Widgets-Speakers_List';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Speakers_List widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Eventium Speakers_List', 'eventium' );
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Speakers_List widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Speakers_List widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return array( 'eventium-widget-blocks' );
	}
	
	/**
	 * Register Eventium_Elementor_Global_Widgets_Speakers_List widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__( 'Block Speakers_List', 'eventium' ),
			)
		);
		
		$this->add_control(
			'title',
			array(
				'label'       => esc_html__( 'Title:', 'eventium' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Add your title', 'eventium' ),
				'label_block' => true,
				'default' => esc_html__('Speakres','eventium')
			)
		);
		
		$this->add_control(
			'categories_selected',
			array(
				'label'     => esc_html__( 'Select Speakers:', 'eventium' ),
				'type'      => Controls_Manager::SELECT2,
				'options'   => eventium_get_speaker_post_options(),
				'multiple' => true,
			)
		);


		$this->end_controls_section();

	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Speakers_List widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render() {
		$title = $this->get_settings( 'title' );
		$categories_selected = $this->get_settings( 'categories_selected' );
		if($categories_selected!=''){
		$posts_number = count($categories_selected);
		$args = array(
			'posts_per_page'      => $posts_number,
			'post_type'           => 'speaker',
			'ignore_sticky_posts' => true,

		);

		// Display from the category selected
		
		
			
		$args[ 'post__in' ] = $categories_selected;
		
		
		$query = new \WP_Query($args);
		
		?>

		<div class="schedule-speakers">
		<h2 class="heading2"><?php echo esc_html($title); ?></h2>
		<div class="row">
		<?php $i=1; while($query->have_posts()) : $query->the_post(); ?>

		
			<?php if($i%4==1 or $i==1){ ?>
			<div class="col-md-6">
				<ul class="speaker-list">
			<?php } ?>
					
					<li>
						<?php the_post_thumbnail('thumbnail');?>
						<span><?php the_title(); ?></span>
					</li>

			<?php if($i%4==0 or $i == $query->post_count){ ?>    
	             
				</ul>
			</div>
			<?php } ?>
		
		<?php $i++; endwhile; ?>
			<?php wp_reset_postdata(); ?>
			</div>
		</div>
		<?php  } ?>
<?php 
}
}
?>