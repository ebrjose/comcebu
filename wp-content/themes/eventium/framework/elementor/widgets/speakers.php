<?php
/**
 * Eventium Elementor Global Widget Title.
 *
 * @package    Nunforest
 * @subpackage Eventium 
 * @since      Eventium  1.0
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	return; // Exit if it is accessed directly
}

class Eventium_Elementor_Global_Widgets_Speakers extends Widget_Base {

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget name.
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'Eventium-Global-Widgets-Speakers';
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget title.
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Eventium Speakers', 'eventium' );
	}

	/**
	 * Retrieve Eventium_Elementor_Global_Widgets_Title widget icon.
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-type-tool';
	}

	/**
	 * Retrieve the list of categories the Eventium_Elementor_Global_Widgets_Title widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return array( 'eventium-widget-blocks' );
	}
	
	/**
	 * Register Eventium_Elementor_Global_Widgets_Title widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Widget title section
		$this->start_controls_section(
			'section_arc_featured_posts_block_1_title_manage',
			array(
				'label' => esc_html__( 'Speakers', 'eventium' ),
			)
		);

		$this->add_control(
			'widget_number',
			array(
				'label'       => esc_html__( 'Number of Posts:', 'eventium' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Add your number of posts', 'eventium' ),
				'label_block' => true,
				'default'     => 6
			)
		);
		$this->add_control(
			'order',
			array(
				'label'       => esc_html__( 'Sort Posts:', 'eventium' ),
				'type'        => Controls_Manager::SELECT,
				'placeholder' => esc_html__( 'Sort posts by', 'eventium' ),
				'default' => 'default',
				'options' => array(
					'default'     => esc_html__( 'Default', 'eventium' ),
					'ASC'     => esc_html__( 'ASC', 'eventium' ),
					'DESC' => esc_html__( 'DESC', 'eventium' ),
				),
			)
		);

		$this->add_control(
			'offset_posts_number',
			array(
				'label' => esc_html__( 'Offset Posts:', 'eventium' ),
				'type'  => Controls_Manager::TEXT,

			)
		);

		$this->add_control(
			'thumbnai_size',
			array(
				'label'       => esc_html__( 'Thumbnail Size.  Example: eventium-team, large, thumbnail...:', 'eventium' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Add your thumbnail size.', 'eventium' ),
				'default'     => '',
			)
		);

		$this->add_control(
			'show_apply',
			[
				'label' => esc_html__( 'Show Apply Link', 'eventium' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'label_on' => esc_html__( 'Show', 'eventium' ),
				'label_off' => esc_html__( 'Hide', 'eventium' ),
				'return_value' => 'yes',
			]
		);
		
		$this->add_control(
			'apply_link',
			array(
				'label' => esc_html__( 'Apply Link:', 'eventium' ),
				'type'  => Controls_Manager::TEXT,
				'default' => '#',
				'condition' => array(
					'show_apply' => 'yes',
				),

			)
		);
		$this->add_control(
			'apply_text',
			array(
				'label' => esc_html__( 'Apply Text:', 'eventium' ),
				'type'  => Controls_Manager::TEXT,
				'default' => esc_html__('Apply to be a speaker','eventium'),
				'condition' => array(
					'show_apply' => 'yes',
				),

			)
		);

		$this->add_control(
			'blog_type',
			array(
				'label'   => esc_html__( 'Speakers Layout Style:', 'eventium' ),
				'type'    => Controls_Manager::SELECT,
				'default' => '1',
				'options' => array(
					'1' => esc_attr__( 'Style 1', 'eventium' ),
					'2' => esc_attr__( 'Style 2', 'eventium' ),
				),
			)
		);

		


		$this->end_controls_section();
	}

	/**
	 * Render Eventium_Elementor_Global_Widgets_Title widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @access protected
	 */
	protected function render() {
		$posts_number        = $this->get_settings( 'widget_number' );
		$order        = $this->get_settings( 'order' );
		$offset_posts_number = $this->get_settings( 'offset_posts_number' );
		$blog_type = $this->get_settings( 'blog_type' );
		$show_apply = $this->get_settings( 'show_apply' );
		$apply_link = $this->get_settings( 'apply_link' );
		$apply_text = $this->get_settings( 'apply_text' );
		$thumbnai_size = $this->get_settings( 'thumbnai_size' );

		 if ( get_query_var('paged') ) {
	    $paged = get_query_var('paged');
	    } elseif ( get_query_var('page') ) {
	        $paged = get_query_var('page');
	    } else {
	        $paged = 1;
	    }   

		$args = array(
			'posts_per_page'      => $posts_number,
			'post_type'           => 'speaker',
			'ignore_sticky_posts' => true,
			
		);

		

		// Offset the posts
		if ( ! empty( $offset_posts_number ) ) {
			$args[ 'offset' ] = $offset_posts_number;
		}

		if ( $order!='default' ) {
			$args[ 'order' ] = $order;
		}

		
		$query = new \WP_Query($args); ?>

		<?php if($blog_type=='2'){ ?>
			<div class="speakers-box">
			<?php $i=1; while($query->have_posts()) : $query->the_post(); ?>

				<?php if($i%2==1 or $i==1){ ?>
		            <div class="row">
		        <?php } ?>  

					<div class="col-lg-6">
						<div class="speaker-post">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail($thumbnai_size); ?></a>
							<div class="speaker-content">
								<h2 class="heading2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<?php $id = get_the_ID(); ?>
								<?php if(get_post_meta($id, '_eventium_job', true)!=''){ ?>
									<span><?php echo esc_html(get_post_meta($id, '_eventium_job', true)); ?></span>
								<?php } ?>
								<p><?php echo esc_html(eventium_excerpt($limit=20)); ?></p>
								<ul class="social-links">
								    <?php if(get_post_meta($id, '_eventium_facebook', true)!=''){ ?>
									<li><a class="facebook" href="<?php echo esc_url(get_post_meta($id, '_eventium_facebook', true)); ?>"><i class="fa fa-facebook"></i></a></li>
									<?php } ?>
									<?php if(get_post_meta($id, '_eventium_twitter', true)!=''){ ?>
									<li><a class="twitter" href="<?php echo esc_url(get_post_meta($id, '_eventium_twitter', true)); ?>"><i class="fa fa-twitter"></i></a></li>
									<?php } ?>
									<?php if(get_post_meta($id, '_eventium_linkedin', true)!=''){ ?>
									<li><a class="linkedin" href="<?php echo esc_url(get_post_meta($id, '_eventium_linkedin', true)); ?>"><i class="fa fa-linkedin"></i></a></li>
									<?php } ?>
									<?php if(get_post_meta($id, '_eventium_instagram', true)!=''){ ?>
									<li><a class="personal-web" href="<?php echo esc_url(get_post_meta($id, '_eventium_instagram', true)); ?>"><i class="fa fa-instagram"></i></a></li>
									<?php } ?>
									<?php if(get_post_meta($id, '_eventium_pinterest', true)!=''){ ?>
									<li><a class="personal-web" href="<?php echo esc_url(get_post_meta($id, '_eventium_pinterest', true)); ?>"><i class="fa fa-pinterest"></i></a></li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					

				<?php if($i%2==0 or $i == $query->post_count){ ?>    
		              </div>
		            <?php } ?>
				
				<?php $i++; endwhile; ?>

				</div>
			<?php wp_reset_postdata(); ?>

		<?php }elseif($blog_type=='3'){ ?>

			
			<?php wp_reset_postdata(); ?>

		<?php }else{ ?>

		 	<div class="speakers-box">

				<?php $i=1; while($query->have_posts()) : $query->the_post(); ?>

					<?php if($i%4==1 or $i==1){ ?>
			            <div class="row">
			        <?php } ?>  

					<div class="col-lg-3 col-md-4 col-sm-6">
						<div class="speaker-post">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail($thumbnai_size); ?></a>
							<div class="speaker-content">
								<h2 class="heading2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<?php $id = get_the_ID(); ?>
								<?php if(get_post_meta($id, '_eventium_job', true)!=''){ ?>
									<span><?php echo esc_html(get_post_meta($id, '_eventium_job', true)); ?></span>
								<?php } ?>
								
									<ul class="social-links">
									    <?php if(get_post_meta($id, '_eventium_facebook', true)!=''){ ?>
										<li><a class="facebook" href="<?php echo esc_url(get_post_meta($id, '_eventium_facebook', true)); ?>"><i class="fa fa-facebook"></i></a></li>
										<?php } ?>
										<?php if(get_post_meta($id, '_eventium_twitter', true)!=''){ ?>
										<li><a class="twitter" href="<?php echo esc_url(get_post_meta($id, '_eventium_twitter', true)); ?>"><i class="fa fa-twitter"></i></a></li>
										<?php } ?>
										<?php if(get_post_meta($id, '_eventium_linkedin', true)!=''){ ?>
										<li><a class="linkedin" href="<?php echo esc_url(get_post_meta($id, '_eventium_linkedin', true)); ?>"><i class="fa fa-linkedin"></i></a></li>
										<?php } ?>
										<?php if(get_post_meta($id, '_eventium_instagram', true)!=''){ ?>
										<li><a class="personal-web" href="<?php echo esc_url(get_post_meta($id, '_eventium_instagram', true)); ?>"><i class="fa fa-instagram"></i></a></li>
										<?php } ?>
										<?php if(get_post_meta($id, '_eventium_pinterest', true)!=''){ ?>
										<li><a class="personal-web" href="<?php echo esc_url(get_post_meta($id, '_eventium_pinterest', true)); ?>"><i class="fa fa-pinterest"></i></a></li>
										<?php } ?>
									</ul>
								
							</div>
						</div>
					</div>
					<?php if($show_apply=='yes'){ ?>
						<?php if($i==$posts_number or $i == $query->post_count){ ?>
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="speaker-apply">
									<h2 class="heading2"><a href="<?php echo esc_url($apply_link); ?>"><?php echo esc_html($apply_text); ?></a></h2>
								</div>
							</div>
						<?php } ?>
					<?php } ?>
					<?php if($i%4==0 or $i == $query->post_count){ ?>    
		              </div>
		            <?php } ?>
				
				<?php $i++; endwhile; ?>
				
				<?php wp_reset_postdata(); ?>
			</div>

		<?php } ?>

		 
		<?php 

	}
}
