<div class="blog-post<?php if(!has_post_thumbnail()){ ?> text-post<?php } ?>">
	<?php the_post_thumbnail('grid');?>

	<div class="post-content">
		<span class="date-comments"><?php the_time(get_option( 'date_format' )); ?>, <?php
             comments_popup_link( esc_html__('0 Comments','eventium'), esc_html__('1 Comment','eventium'), esc_html__('% Comments','eventium'), '',  esc_html__('Comment off','eventium'));
            ?></span>
		<h2 class="heading2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php the_excerpt(); ?>
	</div>

</div>
