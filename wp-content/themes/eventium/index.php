<?php get_header(); ?>
<!-- blog-section 
			================================================== -->
		<section class="blog-section">
			<div class="container">
				<div class="title-section">
					<div class="row">
						<div class="col-lg-4">
							
							<?php 
								$title = get_theme_mod( 'blog_title','Latest news');
								$subtitle = get_theme_mod( 'blog_subtitle');
							?>
							<?php if($subtitle!=''){ ?>
								<span><?php echo esc_html($subtitle);  ?></span>
							<?php } ?>
							<?php if(is_category()){ ?>
								<h1 class="heading1"><?php single_cat_title( '', true ); ?></h1>

							<?php }elseif(is_archive()){ ?>
								<h1 class="heading1"><?php the_archive_title(); ?></h1>
							<?php }elseif(is_404()){ ?>
								<h1 class="heading1"><?php esc_html_e('404 Error','eventium'); ?></h1>
							<?php }elseif (is_tag()) { ?>
								<h1 class="heading1"><?php single_tag_title();?></h1>
							<?php }elseif (is_search()) { ?>
								<h1 class="heading1"><?php esc_html_e('Search results for','eventium'); ?>: '<?php echo esc_attr( get_search_query() ); ?>'</h1>
							<?php }else{ ?>
								<h1 class="heading1"><?php echo esc_html($title);?></h1>
							<?php } ?>
						</div>
						<div class="col-lg-8">
							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<div class="blog-box">
							
							<?php 
                            if(have_posts()) :
                                            while(have_posts()) : the_post(); 
                            ?>
                            <?php get_template_part( 'content', ( post_type_supports( get_post_type(), 'post-formats' ) ? get_post_format() : get_post_type() ) ); ?>
                            <?php endwhile; ?>
                            
                            <?php else: ?>
	                            <div class="blog-post not-found">
	                                    <h2 class="heading2"><?php esc_html_e('Nothing Found Here!','eventium'); ?></h2>
	                                    <p ><?php esc_html_e('Search with other keyword:', 'eventium') ?></p>
	                                    
	                                     <?php get_search_form(); ?>
	                                    
	                            </div>
	                        <?php endif; ?>

							<div class="pagination-box">
								<?php eventium_pagination($prev = esc_html__('prev','eventium'), $next = esc_html__('next','eventium'), $pages=''); ?>
								
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						
						<?php get_sidebar(); ?>

					</div>
				</div>
			</div>
		</section>
		<!-- End blog section -->

<?php get_footer(); ?>