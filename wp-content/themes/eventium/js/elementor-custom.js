(function($){
	"use strict";
	function Resize() {
		$(window).trigger('resize');
	}
	$(window).on('elementor/frontend/init', function () {

		/*-------------------------------------------------*/
		/* =  preloader function
		/*-------------------------------------------------*/
		
		var mainDiv = $('#container'),
			preloader = $('.preloader');

		preloader.fadeOut(400, function(){
			mainDiv.delay(400).addClass('active');
		});

    	elementorFrontend.hooks.addAction('frontend/element_ready/Eventium-Global-Widgets-Blog.default', function(){
    		var $container=$('.iso-call');
			var $filter=$('.filter');

			try{
				$container.imagesLoaded( function(){
					$container.trigger('resize');
					$container.isotope({
						filter:'*',
						layoutMode:'masonry',
						animationOptions:{
							duration:750,
							easing:'linear'
						}
					});

					setTimeout(Resize, 1500);
				});
			} catch(err) {
			}
    	});
    });

    $(window).on('elementor/frontend/init', function () {
    	elementorFrontend.hooks.addAction('frontend/element_ready/Eventium-Global-Widgets-Countdown.default', function(){
    		if ($("#clock").length){
				var count_d = $('#clock').attr('data-date');
				
				$('#clock').countdown(count_d, function(event) {
					var $this = $(this);
					switch(event.type) {
						case "seconds":
						case "minutes":
						case "hours":
						case "days":
						case "daysLeft":
							$this.find('span#'+event.type).html(event.value);
							break;
						case "finished":
							$this.hide();
							break;
					}
				});
			}
    	});
    	
    	elementorFrontend.hooks.addAction('frontend/element_ready/Eventium-Global-Widgets-Statistic.default', function(){
    		$('.statistic-post').appear(function() {
				$('.timer').countTo({
					speed: 4000,
					refreshInterval: 60,
					formatter: function (value, options) {
						return value.toFixed(options.decimals);
					}
				});
			});
    	});
    	
    	elementorFrontend.hooks.addAction('frontend/element_ready/Eventium-Global-Widgets-Blog-Carousel.default', function(){
    		var owlWrap = $('.owl-wrapper');

			if (owlWrap.length > 0) {

				if (jQuery().owlCarousel) {
					owlWrap.each(function(){

						var carousel= $(this).find('.owl-carousel'),
							dataNum = $(this).find('.owl-carousel').attr('data-num'),
							dataNum2,
							dataNum3;

						if ( dataNum == 1 ) {
							dataNum2 = 1;
							dataNum3 = 1;
						} else if ( dataNum == 2 ) {
							dataNum2 = 2;
							dataNum3 = dataNum - 1;
						} else {
							dataNum2 = dataNum - 1;
							dataNum3 = dataNum - 2;
						}

						carousel.owlCarousel({
							autoPlay: 10000,
							navigation : true,
							items : dataNum,
							itemsDesktop : [1199,dataNum2],
							itemsDesktopSmall : [991,dataNum3],
							itemsTablet : [768, dataNum3],
						});

					});
				}
			}
    	});
    	
    	elementorFrontend.hooks.addAction('frontend/element_ready/Eventium-Global-Widgets-Gmap.default', function(){
    		if ($("#map").length){
				var map_latitude = Number($('#map').attr('data-latitude'));
				var map_longitude = Number($('#map').attr('data-longitude'));
				var map_zoom = parseInt($('#map').attr('data-zoom'));
				var map_marker = $('#map').attr('data-marker');
				//alert(map_marker);
				var fenway = [map_longitude,map_latitude]; //Change a map coordinate here!
				var markerPosition = [map_longitude,map_latitude]; //Change a map marker here!
				$('#map').gmap3({
						center: fenway,
						zoom: map_zoom,
						scrollwheel: false,
						mapTypeId : google.maps.MapTypeId.ROADMAP
					})
					.marker({
						position: markerPosition,
						icon: map_marker
				});
			}
    	});

    });

$(document).ready(function($) {



});

})(jQuery);