<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 *
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
    return;
?>
<?php
$aria_req = ( $req ? " aria-required='true'" : '' );
$comment_args = array(
    'title_reply'=> esc_html__('Leave a Comment','eventium'),
    'fields' => apply_filters( 'comment_form_default_fields', array(
        'author' => '<div class="row">
            <div class="col-md-6">
                
                <input type="text" name="author" placeholder="'.esc_attr__('Name','eventium').'*"  id="name" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . ' />
            </div>
        ',
        'email' => '<div class="col-md-6">
            
                <input id="mail" placeholder="'.esc_attr__('Email','eventium').'*" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" ' . $aria_req . ' />
            </div></div>
        ',
        'url' => '
            <input id="website" name="url" placeholder="'.esc_attr__('Website','eventium').'" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '"  />
        	
        '
    )),
    'comment_field' => '<textarea  rows="7" id="comment" placeholder="'.esc_attr__('Comments','eventium').'*" name="comment"'.$aria_req.'></textarea>',
    'label_submit' => esc_html__('Post Comment', 'eventium'),
    'comment_notes_after' => '',
);
?>

<?php if(get_comments_number()>0){ ?>
<div class="comment-area-box">

    <h2 class="heading2"><?php comments_number( esc_html__('0 Comments','eventium'), esc_html__('1 Comments','eventium'), esc_html__('% Comments','eventium') ); ?></h2>
    <?php if(get_comment_pages_count()>0){ ?>
        <ul class="comment-tree">
            <?php wp_list_comments('callback=eventium_theme_comment'); ?>
        </ul>
    <?php } ?>
    <?php
    // Are there comments to navigate through?
    if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
        ?>
        <div class="navigation comment-navigation" role="navigation">
            
            <div class="previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'eventium' ) ); ?></div>
            <div class="next right"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'eventium' ) ); ?></div>
        </div><!-- .comment-navigation -->
    <?php endif; // Check for comment navigation ?>

    <?php if ( ! comments_open() && get_comments_number() ) : ?>
        <p class="no-comments"><?php esc_html_e( 'Comments are closed.' , 'eventium' ); ?></p>
    <?php endif; ?>
</div>
<?php } ?>
<div class="contact-form-box">
    <?php if('open' == $post->comment_status){ ?>
        <?php comment_form($comment_args); ?>
    <?php } ?>
</div>

