<?php
/*
*Arch Decor functions and definitions
*/

define('EVENTIUM_PARENT_DIR', get_template_directory());
define('EVENTIUM_CHILD_DIR', get_stylesheet_directory());

define('EVENTIUM_INCLUDES_DIR', EVENTIUM_PARENT_DIR . '/framework');
define('EVENTIUM_CSS_DIR', EVENTIUM_PARENT_DIR . '/css');
define('EVENTIUM_JS_DIR', EVENTIUM_PARENT_DIR . '/js');
define('EVENTIUM_LANGUAGES_DIR', EVENTIUM_PARENT_DIR . '/languages');

define('EVENTIUM_WIDGETS_DIR', EVENTIUM_INCLUDES_DIR . '/widgets');
define('EVENTIUM_ELEMENTOR_DIR', EVENTIUM_INCLUDES_DIR . '/elementor');
define('EVENTIUM_ELEMENTOR_WIDGETS_DIR', EVENTIUM_ELEMENTOR_DIR . '/widgets');

/**
 * Define URL Location Constants
 */
define('EVENTIUM_PARENT_URL', get_template_directory_uri());
define('EVENTIUM_CHILD_URL', get_stylesheet_directory_uri());

define('EVENTIUM_INCLUDES_URL', EVENTIUM_PARENT_URL . '/framework');
define('EVENTIUM_CSS_URL', EVENTIUM_PARENT_URL . '/css');
define('EVENTIUM_JS_URL', EVENTIUM_PARENT_URL . '/js');
define('EVENTIUM_LANGUAGES_URL', EVENTIUM_PARENT_URL . '/languages');

define('EVENTIUM_WIDGETS_URL', EVENTIUM_INCLUDES_URL . '/widgets');
define('EVENTIUM_ELEMENTOR_URL', EVENTIUM_INCLUDES_URL . '/elementor');
define('EVENTIUM_ELEMENTOR_WIDGETS_URL', EVENTIUM_ELEMENTOR_URL . '/widgets');

/** Add the Elementor compatibility file */
if (defined('ELEMENTOR_VERSION')) {
  require_once(EVENTIUM_ELEMENTOR_DIR . '/elementor.php');
}

if (class_exists('Menu_Item_Custom_Fields')) {
  require_once EVENTIUM_INCLUDES_DIR  . '/menu-item-custom-fields.php';
}

require_once EVENTIUM_INCLUDES_DIR . '/meta-box.php';
require_once EVENTIUM_INCLUDES_DIR . '/import.php';
require_once EVENTIUM_INCLUDES_DIR . '/custom-functions.php';
require_once EVENTIUM_INCLUDES_DIR . '/customize.php';
require_once EVENTIUM_INCLUDES_DIR . '/widgets/popular.php';
require_once EVENTIUM_INCLUDES_DIR . '/eventium_bootstrap_navwalker.php';

function eventium_setup()
{
  /*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/eventium
	 * If you're building a theme based on Arch Decor, use a find and replace
	 * to change 'eventium' to the name of your theme in all the template files.
	 */
  load_theme_textdomain('eventium');

  // Add default posts and comments RSS feed links to head.
  add_theme_support('automatic-feed-links');

  /*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
  add_theme_support('title-tag');

  /*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
  add_theme_support('post-thumbnails');
  add_image_size('eventium-grid', 370, 240, true);
  add_image_size('eventium-team', 540, 360, true);

  add_theme_support('post-formats', array(
    'link', 'quote'
  ));


  // This theme uses wp_nav_menu() in two locations.
  register_nav_menus(array(
    'primary'    => esc_html__('Primary Menu', 'eventium'),
    'onepage'    => esc_html__('One Page Menu', 'eventium'),
  ));

  /*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
  add_theme_support('html5', array(
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ));



  // Add theme support for Custom Logo.
  add_theme_support('custom-logo', array(
    'width'       => 140,
    'height'      => 30,
    'flex-width'  => true,
  ));

  // Add theme support for selective refresh for widgets.
  add_theme_support('customize-selective-refresh-widgets');

  /*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
  add_theme_support('woocommerce');
}
add_action('after_setup_theme', 'eventium_setup');

function eventium_excerpt($limit)
{
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt) >= $limit) {
    array_pop($excerpt);
    $excerpt = implode(" ", $excerpt) . '...';
  } else {
    $excerpt = implode(" ", $excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`', '', $excerpt);
  return $excerpt;
}



/**
 * Display custom color CSS.
 */
function arena_colors_css_wrap()
{
  global  $wp_query;
  $main_color = get_theme_mod('main_color', '#5649c0');
  $main_color2 = get_theme_mod('main_color2', '#2ecc71');
  $header_img = get_header_image();
?>
  <style type="text/css" id="custom-theme-colors">
    <?php
    $css = "
                .dropdown{
                  border-top-color: $main_color;
                }

               section.schedule-section .schedule-block h2, .collapse-box .panel-default > .panel-heading a{
                  border-bottom-color: $main_color;
                }
                .owl-theme .owl-controls .owl-page.active span, .owl-theme .owl-controls.clickable .owl-page:hover span,.collapse-box .panel-default > .panel-heading a:before{
                  border-color: $main_color;
                }

               section.speaking-section .speaker-post .speaker-content span,section.speaking-section .speaker-post .speaker-content ul.social-links li a:hover,
        section.speaking-section3 .speakers-box .speaker-post .speaker-content ul.social-links li a:hover,section.speaking-section .speaker-apply h2 a,.pagination-box ul.page-list li a:hover, section.news-section2 .blog-post a.continue-read,section.blog-section .blog-box .single-post p a,
      .pagination-box ul.page-list li a.active, .pagination-box ul.page-list li span,.sidebar .popular-widget ul.popular-list li .side-content h2 a:hover,section.news-section .news-box .news-post h2 a:hover, section.news-section .news-box .news-post p a ,
      section.speaking-section3 .speakers-box .speaker-apply h2 a,section.speaking-section2 .speaker-post .speaker-content h2 a:hover,section.speaking-single .single-box .single-content .single-data span,section.speaking-single .single-box .single-content .single-connection p span, section.pricing-section .pricing-box .table-box p,section.pricing-section .pricing-box .table-box.enterprise a.default-button:hover,section.pricing-section2 .pricing-box .table-box .price-tab a:hover,section.pricing-section3 .table-box h2,section.clients-section .client-box ul.client-list li a,
      section.clients-section2 .client-box ul.client-list li a,.dropdown > li > a:hover,
      section.clients-section3 .client-box ul.client-list li a,section.clients-section2 .client-box ul.client-list li .client-cont h2,section.conference-experience-section .statistic-box .statistic-post p,section.countdown-section .inner-countdown .comming-part span, section.countdown-section .inner-countdown .comming-part p,section.blog-section .blog-box .blog-post a.continue-read,section.blog-section .blog-box .blog-post ul.post-tags li a,
      section.speaking-section3 .speakers-box .speaker-post .speaker-content span,section.event-banner-section .button-banner-box a:hover,.collapse-box .panel-default > .panel-heading a:before,section.about-us-section .about-us-post i, section.reg-sub-section .subscribe-box .subscribe input[type='submit']:hover, .dropdown > li, section.reg-sub-section .register-box a, .article-box p span, a.more-btn, .conference-features .center-area a, .about-event-box a, section.services-section .services-box .services-post .services-post-content h2, section.services-section .services-box .services-post .services-post-content ul.service-list li, section.services-section2 .center-area a,section.schedule-section .schedule-block h2,section.schedule-section .schedule-block ul.schedule-list li .schedule-cont p.schedule-auth span,section.schedule-section .schedule-block ul.schedule-list li.time-off-item i,section.schedule-section .schedule-block ul.schedule-list li.time-off-item span.time-off,section.schedule-section2 .schedule-box .schedule-tabs .nav-tabs a[aria-selected='true'],section.schedule-section2 .schedule-box .schedule-tabs .tab-content .schedule-post .schedule-cont p.schedule-auth span,section.schedule-section2 .schedule-box .schedule-tabs .tab-content .schedule-post.time-off-item span.time-off,section.schedule-section3 .schedule-block h1,section.schedule-section3 .schedule-block .schedule-content ul.schedule-list li, section.schedule-section3 .schedule-block .schedule-speakers ul.speaker-list li span, section.agenda-section .schedule-box ul.schedule-list li .schedule-cont p.schedule-auth span, .collapse-box .panel-default > .panel-heading a, section.event-banner-section .subscribe-banner-box .subscribe input[type='submit']:hover, section.blog-section .blog-box .prev-next-post .other-post .post-content > a,  .comment-area-box > ul li .comment-box .comment-content h4 span, .comment-area-box > ul li .comment-box .comment-content a, .contact-form-box #comment-form input[type='submit']:hover, section.contact-section .contact-post .contact-post-content span, section.contact-section2 .places-box .places-post a,  section.contact-section2 .contact-info .inner-contact-info p span, section.event-banner-section2 h1, section.event-banner-section3 .subscribe-banner-box .subscribe input[type='submit']:hover, section.contact-section3 .contact-info p i, footer .up-footer .footer-widget .subscribe input[type='submit']:hover{
                    color: $main_color;
                }
                 input[type='submit']:hover, section.contact-section3 .contact-info p.large-ico i, section.contact-section3 #contact-form input[type='submit']:hover, section.contact-section4 .join-newsletter input[type='submit']:hover, section.blog-section .blog-box .single-post blockquote,section.blog-section .blog-box.iso-call.with-sidebar .blog-post.quote-post blockquote,  section.contact-section2 #contact-form input[type='submit']:hover, section.event-banner-section2 h1:after,.collapse-box .panel-default > .panel-heading a.collapsed,section.schedule-section2 .schedule-box .schedule-tabs .tab-content .schedule-post i,section.schedule-section2 .schedule-box .schedule-tabs .nav-tabs a.nav-item, section.services-section2 .services-box .services-post,  section.subscribe-section .subscribe-box input[type='submit']:hover, section.reg-sub-section .register-box a:hover, a.default-button:hover, .navbar-nav > li.active > a:before, .navbar-nav > li.current-menu-parent > a:before, header.fourth-style .navbar-nav > li > a.active:before{
                  background: $main_color;
                }
                  section.schedule-section2 .schedule-box .schedule-tabs .nav-tabs a.nav-item, a.button-one, section.reg-sub-section .subscribe-box .subscribe input[type='submit'], section.register-section .register-box #register-form input[type='submit'], section.subscribe-section .subscribe-box input[type='submit'], section.agenda-section .schedule-box ul.schedule-list li.time-off-item i, section.blog-section .blog-box.iso-call.with-sidebar .blog-post.link-post div, section.statistic-section .statistic-box p.info-line a, .contact-form-box #comment-form input[type='submit'],section.contact-section2 #contact-form input[type='submit'], section.contact-section3 #contact-form input[type='submit'], section.contact-section4 .join-newsletter input[type='submit'], footer .up-footer .footer-widget .subscribe input[type='submit'], a.default-button {
                  background: $main_color2;
                }
                   .pagination-box .prev:hover,
  .pagination-box .next:hover, .pagination-box .prev a:hover,
  .pagination-box .next a:hover, .sidebar .category-widget ul li a:hover,  section.news-section2 .blog-post h2 a:hover, section.speaking-section2 .speaker-post .speaker-content span, section.speaking-section2 .speaker-post .speaker-content ul.social-links li a:hover , section.speaking-single .single-box .single-image a:hover , section.speaking-single .single-box .single-content .single-connection ul.social-links li a:hover, section.speaking-single .single-box .single-content .single-skills .skills-progress div.meter p, section.speaking-section .speaker-post .speaker-content h2 a:hover,
        section.speaking-section3 .speakers-box .speaker-post .speaker-content h2 a:hover, section.schedule-section2 .schedule-box .schedule-tabs .tab-content .schedule-post .schedule-cont span.time, section.agenda-section .schedule-box ul.schedule-list li span.time, .conference-features .conference-post .inner-box i, section.services-section2 .services-box .services-post i, section.schedule-section .schedule-block ul.schedule-list li span.time, section.schedule-section .schedule-text-ban p span, section.blog-section .blog-box .blog-post h2 a:hover, section.blog-section .blog-box .prev-next-post .other-post .post-content h2 a:hover, .comment-area-box > ul li .comment-box .comment-content a:hover, section.contact-section .contact-info .inner-contact-info i, section.contact-section2 .contact-info .inner-contact-info p i, section.contact-section4 .contact-info p i, .widget_recent_entries ul li a:hover, .widget_recent_comments ul li a:hover, .widget_archive ul li a:hover, .widget_categories ul li a:hover, .widget_meta ul li, .widget_pages ul li a:hover, .widget_rss ul li a:hover, .widget_nav_menu ul li a:hover, .product-categories li a:hover, .widget_featured_posts ul li a:hover, input[type='submit']{
                  color: $main_color2;
                }

            .navbar-nav > li > a:hover,
  .navbar-nav > li > a.active{
                  color: $main_color2 !important;
                }
                section.pricing-section2 .pricing-box .table-box .list-tab ul.pricing-list li:before, section.pricing-section3 .table-box ul.pricing-list li:before{
                  border-bottom-color: $main_color2;
                }
                section.pricing-section2 .pricing-box .table-box .list-tab ul.pricing-list li:before, section.pricing-section3 .table-box ul.pricing-list li:before{
                  border-right-color: $main_color2
                }
                section.statistic-section .statistic-box p.info-line a{
                  border-color: $main_color2;
                }
            ";
    $id = $wp_query->get_queried_object_id();
    $bg = get_post_meta($wp_query->get_queried_object_id(), "_arena_page_banner", true);
    if ($bg != '') {
      $css .= ".page-id-$id section.page-banner-section{
                    background: url($bg) !important;
                }";
    }
    if ($header_img != '') {
      $css .= "section.page-banner-section{
                    background-image: url($header_img) !important;
                }";
    }
    echo do_shortcode($css);
    ?>
  </style>
<?php }
add_action('wp_head', 'arena_colors_css_wrap');



/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function eventium_widgets_init()
{

  register_sidebar(array(
    'name'          => esc_html__('Blog Sidebar', 'eventium'),
    'id'            => 'sidebar-1',
    'description'   => esc_html__('Add widgets here to appear in your sidebar on blog posts and archive pages.', 'eventium'),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="heading2 title-sidebar">',
    'after_title'   => '</h2>',
  ));

  register_sidebar(array(
    'name'          => esc_html__('Footer 1', 'eventium'),
    'id'            => 'footer-1',
    'description'   => esc_html__('Add widgets here to appear in your Footer column 1.', 'eventium'),
    'before_widget' => '<section id="%1$s" class="footer-widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="heading2 title-sidebar">',
    'after_title'   => '</h2>',
  ));

  register_sidebar(array(
    'name'          => esc_html__('Footer 2', 'eventium'),
    'id'            => 'footer-2',
    'description'   => esc_html__('Add widgets here to appear in your Footer column 2.', 'eventium'),
    'before_widget' => '<section id="%1$s" class="footer-widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="heading2 title-sidebar">',
    'after_title'   => '</h2>',
  ));

  register_sidebar(array(
    'name'          => esc_html__('Footer 3', 'eventium'),
    'id'            => 'footer-3',
    'description'   => esc_html__('Add widgets here to appear in your Footer column 3.', 'eventium'),
    'before_widget' => '<section id="%1$s" class="footer-widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="heading2 title-sidebar">',
    'after_title'   => '</h2>',
  ));
}
add_action('widgets_init', 'eventium_widgets_init');



/**
 * Enqueue scripts and styles.
 */
function eventium_scripts()
{

  wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
  wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
  wp_enqueue_style('ionicons', get_template_directory_uri() . '/css/ionicons.min.css');
  wp_enqueue_style('magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css');
  wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css');
  wp_enqueue_style('owl-theme', get_template_directory_uri() . '/css/owl.theme.css');
  wp_enqueue_style('jquery-ui', get_template_directory_uri() . '/css/jquery-ui.min.css');
  wp_enqueue_style('flexslider', get_template_directory_uri() . '/css/flexslider.css');
  wp_enqueue_style('eventium-default', get_template_directory_uri() . '/css/style.css');

  // Theme stylesheet.
  wp_enqueue_style('eventium-style', get_stylesheet_uri());

  wp_enqueue_script('jquery-ui-core');
  wp_enqueue_script('masonry');

  wp_enqueue_script('isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array('jquery'), '1.0', true);
  wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array('jquery'), '1.0', true);
  wp_enqueue_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '1.0', true);
  wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.0', true);
  wp_enqueue_script('appear', get_template_directory_uri() . '/js/jquery.appear.js', array('jquery'), '1.0', true);
  wp_enqueue_script('popper', get_template_directory_uri() . '/js/popper.js', array('jquery'), '1.0', true);
  wp_enqueue_script('countdown', get_template_directory_uri() . '/js/countdown.js', array('jquery'), '1.0', true);
  wp_enqueue_script('countTo', get_template_directory_uri() . '/js/jquery.countTo.js', array('jquery'), '1.0', true);
  wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.0', true);
  wp_enqueue_script('maps-google', '//maps.google.com/maps/api/js?key=' . get_theme_mod('map_api', 'AIzaSyCiqrIen8rWQrvJsu-7f4rOta0fmI5r2SI'), array('jquery'), '1.0', true);
  wp_enqueue_script('gmap3', get_template_directory_uri() . '/js/gmap3.min.js', array('jquery'), '1.0', true);
  wp_enqueue_script('eventium-script', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.0', true);
  if (defined('ELEMENTOR_VERSION')) {
    wp_enqueue_script('eventium-elementor', get_template_directory_uri() . '/js/elementor-custom.js', array('jquery'), '1.0', true);
  }

  if (is_singular() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
}
add_action('wp_enqueue_scripts', 'eventium_scripts');

//Custom comment List:
function eventium_theme_comment($comment, $args, $depth)
{

  $GLOBALS['comment'] = $comment; ?>
  <!--=======  COMMENTS =========-->

  <li <?php comment_class(''); ?> id="comment-<?php comment_ID() ?>">

    <div class="comment-box">
      <?php if ($comment->user_id != '0' and get_user_meta($comment->user_id, '_eventium_avatar', true) != '') { ?>
        <?php $image = get_user_meta($comment->user_id, '_eventium_avatar', true); ?>
        <img src="<?php echo esc_attr($image); ?>" />
      <?php } else { ?>
        <?php echo get_avatar($comment); ?>
      <?php } ?>

      <div class="comment-content">
        <?php comment_text() ?>
        <?php if ($comment->comment_approved == '0') : ?>
          <em><?php esc_html_e('Your comment is awaiting moderation.', 'eventium') ?></em>
          <br />
        <?php endif; ?>
        <h4><?php esc_html_e('by', 'eventium'); ?> <span> <?php printf(esc_html__('%s', 'eventium'), get_comment_author_link()) ?> </span></h4>
        <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>
    </div>


  <?php
}

function eventium_option($option)
{
  global $wp_query;


  $id = $wp_query->post->ID;


  $option_name = '_eventium_' . $option;
  $value  = get_post_meta($id, $option_name, true);
  return $value;
}

//pagination
function eventium_pagination($prev = '', $next = '', $pages = '')
{
  global $wp_query, $wp_rewrite;
  $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
  if ($pages == '') {
    global $wp_query;
    $pages = $wp_query->max_num_pages;
    if (!$pages) {
      $pages = 1;
    }
  }
  if (is_front_page() and !is_home()) {
    $curent = (get_query_var('page')) ? get_query_var('page') : 1;
  } else {
    $curent = (get_query_var('paged')) ? get_query_var('paged') : 1;
  }
  $pagination = array(
    'base'       => str_replace(999999999, '%#%', get_pagenum_link(999999999)),
    'format'     => '',
    'current'     => max(1, $curent),
    'total'     => $pages,
    'prev_text' => $prev,
    'next_text' => $next,
    'prev_next' => true,
    'type'      => 'list',
    'end_size'    => 2,
    'mid_size'    => 1
  );
  $return =  paginate_links($pagination);
  echo str_replace("<ul class='page-numbers'>", '<ul class="page-list">', $return); ?>

  <?php }


add_filter('wp_list_categories', 'eventium__span_cat_count');
function eventium__span_cat_count($links)
{
  $links = str_replace('</a> (', '</a> <span>(', $links);
  $links = str_replace(')', ')</span>', $links);
  return $links;
}

/*
Register Fonts
*/

function eventium_fonts_url()
{
  $font_url = '';

  /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
  if ('off' !== _x('on', 'Google font: on or off', 'eventium')) {
    $font_url = add_query_arg('family', urlencode('Lora:400,400i,700,700i|Rubik:300,300i,400,400i,500,500i,700,700i,900,900i&subset=latin,latin-ext'), "//fonts.googleapis.com/css");
  }
  return $font_url;
}


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if (!isset($content_width)) {
  $content_width = 900;
}

/*
Enqueue scripts and styles.
*/
function eventium_font_scripts()
{
  wp_enqueue_style('eventium-cebu-fonts', get_template_directory_uri() . '/css/fonts/fonts.css');
  wp_enqueue_style('eventium-fonts', eventium_fonts_url(), array(), '1.0.0');
}
add_action('wp_enqueue_scripts', 'eventium_font_scripts');



function eventium_elementor_categories()
{
  $output     = array();
  $categories = get_categories();
  $output[] = 'Select';
  foreach ($categories as $category) {
    $output[$category->term_id] = $category->name;
  }

  return $output;
}
function eventium_get_post_options($query_args)
{

  $args = wp_parse_args($query_args, array(
    'post_type'   => 'post',
    'numberposts' => 10,
  ));

  $posts = get_posts($args);

  $post_options = array();
  if ($posts) {
    foreach ($posts as $post) {
      $post_options[$post->ID] = $post->post_title;
    }
  }

  return $post_options;
}


function eventium_get_speaker_post_options()
{
  return eventium_get_post_options(array('post_type' => 'speaker', 'numberposts' => -1));
}
function eventium_get_ticket_post_options()
{
  return eventium_get_post_options(array('post_type' => 'pricing', 'numberposts' => -1));
}


/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme construct for publication on ThemeForest
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

require_once get_template_directory() . '/framework/class-tgm-plugin-activation.php';
add_action('tgmpa_register', 'eventium_register_required_plugins');
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function eventium_register_required_plugins()
{
  /**
   * Array of plugin arrays. Required keys are name and slug.
   * If the source is NOT from the .org repo, then source is also required.
   */
  $plugins = array(
    // This is an example of how to include a plugin from a private repo in your theme.


    array(
      'name'               => esc_html__('QK Register Post Type', 'eventium'),
      'slug'               => 'qk-post_type',
      'source'             => get_template_directory() . '/framework/plugins/qk-post_type.zip',
      'required'           => true,
    ),

    // This is an example of how to include a plugin from the WordPress Plugin Repository.

    array(
      'name'      => esc_html__('Kirki', 'eventium'),
      'slug'      => 'kirki',
      'required'  => true,
    ), array(
      'name'      => esc_html__('Elementor Page Builder', 'eventium'),
      'slug'      => 'elementor',
      'required'  => true,
    ), array(
      'name'      => esc_html__('CMB2', 'eventium'),
      'slug'      => 'cmb2',
      'required'  => true,
    ), array(
      'name'      => esc_html__('Contact Form 7', 'eventium'),
      'slug'      => 'contact-form-7',
      'required'  => true,
    ), array(
      'name'      => esc_html__('One Click Demo Import', 'eventium'),
      'slug'      => 'one-click-demo-import',
      'required'  => true,
    ), array(
      'name'      => esc_html__('MailChimp for WordPress', 'eventium'),
      'slug'      => 'mailchimp-for-wp',
      'required'  => false,
    )
  );

  /**
   * Array of configuration settings. Amend each line as needed.
   * If you want the default strings to be available under your own theme domain,
   * leave the strings uncommented.
   * Some of the strings are added into a sprintf, so see the comments at the
   * end of each line for what each argument will be.
   */
  $config = array(
    'default_path' => '',                      // Default absolute path to pre-packaged plugins.
    'menu'         => 'tgmpa-install-plugins', // Menu slug.
    'has_notices'  => true,                    // Show admin notices or not.
    'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
    'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
    'is_automatic' => false,                   // Automatically activate plugins after installation or not.
    'message'      => '',                      // Message to output right before the plugins table.
    'strings'      => array(
      'page_title'                      => esc_html__('Install Required Plugins', 'eventium'),
      'menu_title'                      => esc_html__('Install Plugins', 'eventium'),
      'installing'                      => esc_html__('Installing Plugin: %s', 'eventium'), // %s = plugin name.
      'oops'                            => esc_html__('Something went wrong with the plugin API.', 'eventium'),
      'notice_can_install_required'     => _n_noop('This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'eventium'), // %1$s = plugin name(s).
      'notice_can_install_recommended'  => _n_noop('This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'eventium'), // %1$s = plugin name(s).
      'notice_cannot_install'           => _n_noop('Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'eventium'), // %1$s = plugin name(s).
      'notice_can_activate_required'    => _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'eventium'), // %1$s = plugin name(s).
      'notice_can_activate_recommended' => _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'eventium'), // %1$s = plugin name(s).
      'notice_cannot_activate'          => _n_noop('Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'eventium'), // %1$s = plugin name(s).
      'notice_ask_to_update'            => _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'eventium'), // %1$s = plugin name(s).
      'notice_cannot_update'            => _n_noop('Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'eventium'), // %1$s = plugin name(s).
      'install_link'                    => _n_noop('Begin installing plugin', 'Begin installing plugins', 'eventium'),
      'activate_link'                   => _n_noop('Begin activating plugin', 'Begin activating plugins', 'eventium'),
      'return'                          => esc_html__('Return to Required Plugins Installer', 'eventium'),
      'plugin_activated'                => esc_html__('Plugin activated successfully.', 'eventium'),
      'complete'                        => esc_html__('All plugins installed and activated successfully. %s', 'eventium'), // %s = dashboard link.
      'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
    )
  );
  tgmpa($plugins, $config);
}
  ?>