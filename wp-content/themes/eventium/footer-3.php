<!-- footer
			================================================== -->
<footer class="third-style">
	<div class="container">

		<p class="copyright-line"><?php echo do_shortcode(get_theme_mod('copyright', '&copy; COMCEBU - ' . date('Y'))); ?></p>

	</div>
</footer>
<!-- End footer -->