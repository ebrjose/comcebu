<footer class="second-style">
			<div class="container">

				<ul class="social-icons">
					<?php if(get_theme_mod( 'facebook' )!=''){ ?>
						<li><a class="facebook" href="<?php echo esc_url(get_theme_mod( 'facebook' )); ?>"><i class="fa fa-facebook"></i></a></li>
					<?php } ?>
					<?php if(get_theme_mod( 'twitter' )!=''){ ?>
						<li><a class="twitter" href="<?php echo esc_url(get_theme_mod( 'twitter' )); ?>"><i class="fa fa-twitter"></i></a></li>
					<?php } ?>
					<?php if(get_theme_mod( 'linkedin' )!=''){ ?>
						<li><a class="linkedin" href="<?php echo esc_url(get_theme_mod( 'linkedin' )); ?>"><i class="fa fa-linkedin"></i></a></li>
					<?php } ?>
					<?php if(get_theme_mod( 'instagram' )!=''){ ?>
						<li><a class="instagram" href="<?php echo esc_url(get_theme_mod( 'instagram' )); ?>"><i class="fa fa-instagram"></i></a></li>
					<?php } ?>
					<?php if(get_theme_mod( 'youtube' )!=''){ ?>
						<li><a class="youtube" href="<?php echo esc_url(get_theme_mod( 'youtube' )); ?>"><i class="fa fa-youtube"></i></a></li>
					<?php } ?>
				</ul>

				<p class="copyright-line"><?php echo do_shortcode(get_theme_mod( 'copyright','&copy; Copyright Eventium Theme - Theme by Nunforest')); ?></p>

			</div>
		</footer>