<?php get_header(); ?>
<?php while(have_posts()) : the_post(); ?>
<!-- blog-section 
			================================================== -->
		<section class="blog-section">
			<div class="container">
				<div class="title-section">
					<span><?php the_time(get_option( 'date_format' )); ?></span>
					<h1 class="heading1"><?php the_title(); ?></h1>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<div class="blog-box">
							<div class="blog-post single-post">
								<?php the_post_thumbnail(); ?>
								<div class="single-content">
									
									<?php the_content(); ?>
				                      <?php
				                        $defaults = array(
				                          'before'           => '<div id="page-links"><strong>'.esc_html__('Page','eventium').': </strong>',
				                          'after'            => '</div>',
				                          'link_before'      => '<span>',
				                          'link_after'       => '</span>',
				                          'next_or_number'   => 'number',
				                          'separator'        => ' ',
				                          'nextpagelink'     => esc_html__( 'Next page','eventium' ),
				                          'previouspagelink' => esc_html__( 'Previous page','eventium' ),
				                          'pagelink'         => '%',
				                          'echo'             => 1
				                        );
				                       ?>
				                      <?php wp_link_pages($defaults); ?>
				                  </div>

								<div class="row">
									
									<div class="col-md-12">
										 <?php if(has_tag()){ ?>
										 	<?php the_tags('<ul class="post-tags"><li>in ',',</li> <li>','</li></ul>'); ?>
										 <?php } ?>
										
									</div>
								</div>

							</div>
							<?php
				              $prev_post = get_adjacent_post(false, '', true);
				              $next_post = get_adjacent_post(false, '', false); 
				              if(get_theme_mod('related_post')==true){ 
				            ?>
							<div class="prev-next-post">
								<div class="row">
									<div class="col-md-6">
										<?php if($prev_post!=null){ ?>
										<div class="other-post prev-post<?php if(!has_post_thumbnail($prev_post->ID)){ ?> no-thumb<?php } ?>">
											<?php echo get_the_post_thumbnail($prev_post->ID, 'thumbnail'); ?>
											<div class="post-content">
												<a href="<?php echo esc_url(get_permalink($prev_post->ID)); ?>"><?php esc_html_e('Prew Post','eventium'); ?></a>
												<?php 
													$title = $prev_post->post_title;
													$n_title  = strlen ($title ); 
													if($n_title>25){ 
														$sub_title = substr($title, 0, 25).'...';
													}else{
														$sub_title = $title;
													}
												?>
												<h2 class="heading2"><a href="<?php echo esc_url(get_permalink($prev_post->ID)); ?>"><?php echo esc_attr($sub_title); ?></a></h2>
												<span><?php echo get_the_time(get_option( 'date_format' ), $prev_post->ID); ?></span>
											</div>
										</div>
										<?php } ?>
									</div>
									<div class="col-md-6">
										<?php if($next_post!=null){ ?>
										<div class="other-post next-post<?php if(!has_post_thumbnail($next_post->ID)){ ?> no-thumb<?php } ?>">
											<?php echo get_the_post_thumbnail($next_post->ID, 'thumbnail'); ?>
											<div class="post-content">
												<a href="<?php echo esc_url(get_permalink($next_post->ID)); ?>"><?php esc_html_e('Next Post','eventium'); ?></a>
												<?php 
													$title = $next_post->post_title;
													$n_title  = strlen ($title ); 
													if($n_title>25){ 
														$sub_title = substr($title, 0, 25).'...';
													}else{
														$sub_title = $title;
													}
												?>
												<h2 class="heading2"><a href="<?php echo esc_url(get_permalink($next_post->ID)); ?>"><?php echo esc_attr($sub_title); ?></a></h2>
												<span><?php echo get_the_time(get_option( 'date_format' ), $next_post->ID); ?></span>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
						<?php } ?>
							<?php if ( comments_open() || get_comments_number() ) {  ?>
			                    <?php comments_template(); ?>   
			                <?php } ?>

							
							<!-- End contact form box -->
						</div>
					</div>
					<div class="col-lg-4">
						
						<?php get_sidebar(); ?>

					</div>
				</div>
			</div>
		</section>
		<!-- End blog section -->
<?php endwhile; ?>

<?php get_footer(); ?>